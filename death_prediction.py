""" Script to do the death prediction based on a testing model and confirmed cases and tests."""

import torch
import torch.nn as nn
import torch.nn.functional as F

# Death filter based on Flaxman
death_filter = torch.tensor([2.3973e-08, 4.3723e-06, 5.0811e-05, 2.5835e-04, 8.2425e-04, 1.9629e-03,
                             3.8389e-03, 6.5244e-03, 9.9860e-03, 1.4096e-02, 1.8658e-02, 2.3437e-02,
                             2.8191e-02, 3.2693e-02, 3.6750e-02, 4.0215e-02, 4.2986e-02, 4.5010e-02,
                             4.6275e-02, 4.6806e-02, 4.6655e-02, 4.5893e-02, 4.4605e-02, 4.2882e-02,
                             4.0815e-02, 3.8493e-02, 3.5996e-02, 3.3399e-02, 3.0765e-02, 2.8148e-02,
                             2.5593e-02, 2.3132e-02, 2.0794e-02, 1.8595e-02, 1.6548e-02, 1.4660e-02,
                             1.2931e-02, 1.1359e-02, 9.9398e-03, 8.6661e-03, 7.5294e-03, 6.5202e-03,
                             5.6286e-03, 4.8443e-03, 4.1574e-03, 3.5581e-03, 3.0373e-03, 2.5863e-03,
                             2.1969e-03, 1.8619e-03, 1.5556e-03, 1.2599e-03, 1.0099e-03, 8.0441e-04,
                             6.3791e-04, 5.0417e-04, 3.9740e-04, 3.1254e-04, 2.4534e-04], dtype=torch.double)
death_filter = torch.flip(death_filter, (0,))
smoothing_filter = torch.tensor([1., 1., 1., 1., 1., 1., 1.], dtype=torch.double) / 7.


class DeathInference(nn.Module):
    def __init__(self, gamma=.2):
        super(DeathInference, self).__init__()
        self.l_filter = death_filter.size(0)
        self.l_smooth = smoothing_filter.size(0)
        self.f_death = torch.reshape(death_filter, (1, 1, -1))
        self.f_smooth = torch.reshape(smoothing_filter, (1, 1, -1))
        self.conv_death = nn.Conv1d(1, 1, self.l_filter, stride=1, padding=0, bias=False)
        self.conv_smooth = nn.Conv1d(1, 1, self.l_smooth, stride=1, padding=0, bias=False)
        self.eps = torch.tensor([1.], dtype=torch.double)
        self.gamma = gamma
        self.min_death = 10

    def forward(self, Y, T, D, alpha, reverseEq):
        D = torch.reshape(D, (1, 1, -1))
        D = torch.max(D, self.eps)
        I_est = reverseEq(Y, T, alpha)
        I_new = I_est[1:] - (1 - self.gamma) * I_est[:-1]
        # Also pad one zero at left because we can only find new infections from day 2 as we use differences
        I_new = F.pad(I_new, (self.l_filter, 0))
        I_new = torch.reshape(I_new, (1, 1, -1))
        D_pred = F.conv1d(I_new, self.f_death)

        D_pred = D_pred * torch.sum(D) / torch.sum(D_pred)
        D_pred = torch.max(D_pred, self.eps)
        return torch.var(torch.log(D[D > self.min_death]) - torch.log(D_pred[D > self.min_death])) / (
                    torch.var(torch.log(D[D > self.min_death])) + 1)
