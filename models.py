# -*- coding: utf-8 -*-
"""
This library gathers the testing statistical models we can compare\
    for policy evaluation and death prediction
Created on Wed Sep  2 15:42:58 2020

@author: besserve
"""

import torch
import numpy as np

import glmtools as gt


def powFun(x, y):
    if isinstance(x, torch.Tensor):
        return torch.pow(x, y)
    else:
        return np.power(x, y)


def expFun(x):
    if isinstance(x, torch.Tensor):
        return torch.exp(x)
    else:
        return np.exp(x)


def logFun(x):
    if isinstance(x, torch.Tensor):
        return torch.log(x)
    else:
        return np.log(x)


def sigmoidFun(x):
    if isinstance(x, torch.Tensor):
        return torch.sigmoid(x)
    else:
        return 1 / (1 + np.exp(-x))


def reluFun(x):
    if isinstance(x, torch.Tensor):
        return torch.relu(x)
    else:
        return np.positive(x)


def stdFun(x):
    if isinstance(x, torch.Tensor):
        return torch.std(x)
    else:
        return np.std(x)


# use a placeholder for self, which is always understood as the first argument inside a class
# forward equation is for now just the multiplicative factor in the exposure
def poissonFwd(selfPH, y, T, alpha):
    return T


# backward equation is the estimate of I based on y,T, alpha, etc...
def poissonRev(selfPH, y, T, alpha):
    return y / T


# trivial constant prediction model
def constantFwd(selfPH, y, T, alpha):
    return 1 / y


# backward equation is the estimate of I based on y,T, alpha, etc...
def constantRev(selfPH, y, T, alpha):
    return 1 + 0 * T


# model based on Y = I(T+alpha)
def additiveFwd(selfPH, y, T, alpha):
    return T + reluFun(alpha[0])
    alpha = torch.clamp(alpha, max=20, min=-20)
    return T + expFun(alpha[0])


def additiveRev(selfPH, y, T, alpha):
    return y / (T + reluFun(alpha[0]))
    alpha = torch.clamp(alpha, max=20, min=-20)
    return y / (T + expFun(alpha[0]))


# model based on quotient Y = I * (T + alpha1) / (T + alpha2)
def quotientFwd(selfPH, y, T, alpha):
    if isinstance(alpha, torch.Tensor):
        return (T + torch.relu(alpha[0])) / (T + torch.relu(alpha[1]))
    return (T + alpha[0]) / (T + alpha[1])


def quotientRev(selfPH, y, T, alpha):
    if isinstance(alpha, torch.Tensor):
        return y * (T + torch.relu(alpha[..., 1])) / (T + torch.relu(alpha[..., 0]))
    return y * (T + alpha[..., 1][0]) / (T + alpha[..., 0][0])


# model based on power Y = I T^alpha
def powerFwd(selfPH, y, T, alpha):
    return powFun(T, sigmoidFun(alpha[0]))


def powerRev(selfPH, y, T, alpha):
    return y / powFun(T, sigmoidFun(alpha[0]))


# for baseline we just have no exposure in Poisson glm, and ue I=Y
def baselineFwd(selfPH, y, T, alpha):
    return 1 + 0 * T


def baselineRev(selfPH, y, T, alpha):
    return y


def poisAlphaFwd(selfPH, y, T, alpha):
    return T / (T + reluFun(alpha[0]))
    alpha = torch.clamp(alpha, max=20, min=-20)
    return T / (T / expFun(alpha[0]) + 1)


def poisAlphaRev(selfPH, y, T, alpha):
    return y / T * (T + reluFun(alpha[0]))
    alpha = torch.clamp(alpha, max=20, min=-20)
    return y / T * (T / expFun(alpha[0]) + 1)


def downdeFwd(selfPH, y, T, alpha):
    return powFun(T, 2) / (T / expFun(alpha[0]) + 1)


def downdeRev(selfPH, y, T, alpha):
    return y / powFun(T, 2) * (T / expFun(alpha[0]) + 1)


def poisRelAlphaFwd(selfPH, y, T, alpha):
    return None


def poisRelAlphaRev(selfPH, y, T, alpha):
    return y * T / (reluFun(T - alpha[0] * y) + (1.e-2) * stdFun(T))


def poisSatFwd(selfPH, y, T, alpha):
    return y * expFun(sigmoidFun((logFun(T) - alpha[..., 0]) / .25) + alpha[..., 1])


def poisSatRev(selfPH, y, T, alpha):
    return y / expFun(sigmoidFun((logFun(T) - alpha[..., 0]) / .25) + alpha[..., 1])


class TestModel:
    """
    This test model class is a template model to allow encapsulated statistical\
        estimation and death prediction for various models
        
        Attributes:
            alpha: value of parameter for testing function (None is default)
            alphaOptim: whether alpha will be optiized or considered fixed (fixed in default)
    """
    params = []

    def __init__(self, name, forwardEq, reverseEq, family, alphaOptim=False, alpha=None):
        self.name = name
        self.forwardEq = forwardEq
        self.reverseEq = reverseEq
        self.alphaOptim = alphaOptim
        self.alpha = alpha

    def infectiousEstimate(self, t, lockdownDates, y, T, excludeInter=[], debias=2):
        if self.alphaOptim:
            ypred, params, paramLabels = self.infectiousPanelEstimate(t, lockdownDates, y, T,
                                                                      excludeIntervals=excludeInter, debias=debias)

        else:
            if isinstance(y, np.ndarray):  # for panel data
                ypred, params, paramLabels = tuple(zip(*[gt.glmIndivFit(y[:, idx], t, T[:, idx], lockdownDates, \
                                                                        self.family, self.forwardEq, excludeInter,
                                                                        alpha=self.alpha) for idx in
                                                         range(y.shape[1])]))
            else:
                if isinstance(t, np.ndarray):
                    if not (np.any(np.array(t.shape) == 1) or len(t.shape) == 1):
                        raise IOError('time array is invalid for country list')
                    t = t.squeeze()
                    t = [t] * len(y)
                if not isinstance(lockdownDates[0], list):
                    lockdownDates = [lockdownDates] * len(y)
                if not isinstance(excludeInter[0], list):
                    excludeInter = [excludeInter] * len(y)

                ypred, params, paramLabels = tuple(zip(*[gt.glmIndivFit(y[idx], t[idx], T[idx], lockdownDates[idx], \
                                                                        self.family, self.forwardEq, excludeInter[idx],
                                                                        alpha=self.alpha) for idx in range(len(y))]))

        return ypred, params, paramLabels

    def infectiousPanelEstimate(self, t, lockdownDates, y, T, excludeIntervals=[], debias=2):
        self.alpha = gt.glmPanelFit(y, t, T, lockdownDates, self.family, self.forwardEq, excludeIntervals,
                                    alphaInit=self.alpha, debias=debias)
        self.alpha = np.minimum(self.alpha, 1e20)
        # run individual fits once alpha is estimated
        ypred, params, paramLabels = tuple(zip(*[gt.glmIndivFit(y[:, idx], t, T[:, idx], lockdownDates,
                                                                self.family, self.forwardEq, excludeIntervals,
                                                                alpha=self.alpha) for idx in range(y.shape[1])]))

        return ypred, params, paramLabels

    def printname(self):
        print(self.name)


class PoissonModel(TestModel):
    alpha = None
    forwardEq = poissonFwd
    reverseEq = poissonRev
    family = 'poisson'
    name = 'Limiting'
    alpha = None
    alphaOptim = False

    def __init__(self):
        pass


class BaselineModel(TestModel):
    alpha = None
    forwardEq = baselineFwd
    reverseEq = baselineRev
    family = 'poisson'
    name = 'Adapted'
    alpha = None
    alphaOptim = False

    def __init__(self):
        pass


class PoisAlphaModel(PoissonModel):
    alpha = np.array([100])
    name = 'Up-saturating'
    forwardEq = poisAlphaFwd
    reverseEq = poisAlphaRev
    family = 'poisson'
    alphaOptim = True

    def __init__(self, alpha):
        self.alpha = alpha


class PoisRelAlphaModel(PoissonModel):
    alpha = np.array([100])
    name = 'poissonRelAlpha'
    forwardEq = poisRelAlphaFwd
    reverseEq = poisRelAlphaRev
    family = 'poisson'
    alphaOptim = True

    def __init__(self, alpha):
        self.alpha = alpha


class AdditiveModel(PoissonModel):
    alpha = np.array([100])
    name = 'Down-saturating'
    forwardEq = additiveFwd
    reverseEq = additiveRev
    family = 'poisson'
    alphaOptim = True

    def __init__(self, alpha):
        self.alpha = alpha


class QuotientModel(PoissonModel):
    alpha = np.array([100, 100])
    name = 'Bi-saturating'
    forwardEq = quotientFwd
    reverseEq = quotientRev
    family = 'poisson'
    alphaOptim = True

    def __init__(self, alpha):
        self.alpha = alpha


class PoisSatModel(PoissonModel):
    alpha = np.array([100, 100])
    name = 'poissonSat'
    forwardEq = poisSatFwd
    reverseEq = poisSatRev
    family = 'poisson'
    alphaOptim = True

    def __init__(self, alpha):
        self.alpha = alpha


class PowerModel(PoissonModel):
    alpha = np.array([100])
    name = 'power'
    forwardEq = powerFwd
    reverseEq = powerRev
    family = 'poisson'
    alphaOptim = True

    def __init__(self, alpha):
        self.alpha = alpha


class ConstantModel(TestModel):
    alpha = np.array([100])
    name = 'constant'
    forwardEq = constantFwd
    reverseEq = constantRev
    family = 'poisson'
    alphaOptim = False

    def __init__(self):
        pass


class DownDecreaseModel(TestModel):
    alpha = np.array([100])
    name = 'Down-decreasing'
    forwardEq = downdeFwd
    reverseEq = downdeRev
    family = 'poisson'
    alphaOptim = True

    def __init__(self, alpha):
        self.alpha = alpha
