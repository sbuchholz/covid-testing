# -*- coding: utf-8 -*-
"""
Created on Thu Sep 10 13:38:09 2020

@author: besserve
"""
import numpy as np

""" Plotting helper for boxplots."""


def simpleBoxMatchedSamp(ax, squerr, labels, title='Mean squared prediction error', testchange=None, y=5., ylim=2.,
                         patch_artist=False):
    bplot = ax.boxplot(squerr, patch_artist=patch_artist, medianprops=dict(color="black"))

    ax.set_title(title)
    if isinstance(squerr, np.ndarray):
        ax.set_xticks(np.arange(squerr.shape[1]) + 1)
        ax.set_xticklabels(labels)
    else:
        ax.set_xticks(np.arange(len(squerr)) + 1)
        ax.set_xticklabels(labels)

    if testchange is not None:
        h = .1
        col = 'k'
        x1, x2 = 1, 2
        ax.plot([x1, x1, x2, x2], [y - 2 * h, y + h, y + h, y], lw=1.5, c=col)
        ax.text((x1 + x2) * .5, y + h, "p=" + str(round(testchange, 4)), ha='center', va='bottom', color=col)
    return bplot


def barplot_annotate_brackets(ax, num1, num2, data, center, height, yerr=None, dh=.05, barh=.05, fs=None,
                              maxasterix=None):
    """ 
    Annotate barplot with p-values.

    :param num1: number of left bar to put bracket over
    :param num2: number of right bar to put bracket over
    :param data: string to write or number for generating asterixes
    :param center: centers of all bars (like plt.bar() input)
    :param height: heights of all bars (like plt.bar() input)
    :param yerr: yerrs of all bars (like plt.bar() input)
    :param dh: height offset over bar / bar + yerr in axes coordinates (0 to 1)
    :param barh: bar height in axes coordinates (0 to 1)
    :param fs: font size
    :param maxasterix: maximum number of asterixes to write (for very small p-values)
    """

    if type(data) is str:
        text = data
    else:
        text = ''
        p = .05

        while data < p:
            text += '*'
            p /= 10.

            if maxasterix and len(text) == maxasterix:
                break

        if len(text) == 0:
            text = 'n. s.'

    lx, ly = center[num1], height[num1]
    rx, ry = center[num2], height[num2]

    if yerr:
        ly += yerr[num1]
        ry += yerr[num2]

    ax_y0, ax_y1 = ax.get_ylim()
    dh *= (ax_y1 - ax_y0)
    barh *= (ax_y1 - ax_y0)

    y = max(ly, ry) + dh

    barx = [lx, lx, rx, rx]
    bary = [y, y + barh, y + barh, y]
    mid = ((lx + rx) / 2, y + barh)
    if not num1 == num2:
        ax.plot(barx, bary, c='black')

    kwargs = dict(ha='center', va='bottom')
    if fs is not None:
        kwargs['fontsize'] = fs

    ax.text(*mid, text, **kwargs)
