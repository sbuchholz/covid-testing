# -*- coding: utf-8 -*-
"""
Created on Wed Sep  2 13:37:02 2020

@author: besserve
"""
""" This script generates figure 3 and the corresponding supplementary figures and calculates the required p-values."""


import pandas as pd
import numpy as np

import seaborn as sns
import matplotlib.pyplot as plt
from datetime import datetime
from scipy.stats import wilcoxon, kruskal

import models as md
import plotutils as pu
from preprocess import preprocess_dataset, set_selection_variable, get_selected_countries
import statutils as su

panelLetter = [['A', 'B', 'C'], ['D', 'E', 'F'], [' ', 'G', 'H']]
stest = wilcoxon

plt.rcParams.update({'font.size': 12})
plt.tight_layout()
fig, ax = plt.subplots(3, 3, figsize=[18, 15])


# use up-saturating model for correction (if *True*), otherwise use (non-saturated) limiting factor model
useUpSatMod = True
# use adapted testing model as baseline (if *False*), otherwise use (non-saturated) limiting factor model
useLimModasBaseline = False
lockdownState = [False, True]
effect_frames = []
p_vals = []
stringency = []
stringency_list = []
test_slopes = []
lockName = ['1st lockdown',
            '2nd lockdown']
pvalstot = []
p_valIndiv = []
grateBaseVals = []
grateCorrecVals = []
for ld in range(2):
    # Parameters to allow switch between first and second lockdown
    sec_lockdown = lockdownState[ld]
    days_before_lockdown = 21
    days_after_lockdown = 31
    date = datetime(2020, 10, 1)
    threshold = 50

    all_data = preprocess_dataset("Countries", local=True)
    # Remove Taiwan as there was no real lockdown and return to normality too fast for regression
    # Remove Thailand from 2nd lockdown because there are very few cases
    # Remove Colombia due to incomplete testing data
    # Remove France from 1st lockdown because of insufficient testing data
    if "Taiwan" in all_data.keys():
        del all_data["Taiwan"]
    del all_data["Colombia"]
    if ld == 1:
        del all_data["Thailand"]
    if ld == 0:
        del all_data["France"]

    # require 7 days before first lockdown
    min_death = 1
    min_index = -7
    set_selection_variable(all_data, min_death, "death")
    if not sec_lockdown:
        set_selection_variable(all_data, min_index, "time", False)

    countries_sel = get_selected_countries(all_data)
    countries_sel = sorted(countries_sel, key=lambda c: c.name)

    # Comparison of lockdown effect and growth rates for Poisson and baseline testing models
    pModel = md.PoissonModel()
    baseModel = md.BaselineModel()

    # remove post lockdown samples while building the lists
    y = []
    T = []
    tax = []
    if not sec_lockdown:
        for count in countries_sel:
            y.append(count.conf[:count.index_end_lockdown] / count.population)
            T.append(count.test[:count.index_end_lockdown] / count.population)
            tax.append(count.t[:count.index_end_lockdown])

            # Create data for the stringencys
            stringency.append(["First lockdown", "Pre-lockdown", np.mean(count.stringency[:-count.t[0]])])
            stringency.append(
                ["First lockdown", "During lockdown", np.mean(count.stringency[-count.t[0]: count.index_end_lockdown])])
            stringency_list.append(np.mean(count.stringency[:-count.t[0]]) - np.mean(
                count.stringency[-count.t[0]: count.index_end_lockdown]))
    else:
        countries_sec_lockdown = []
        for count in countries_sel:
            if count.get_lockdown_index(date, threshold, days_after_lockdown) is not None:
                countries_sec_lockdown.append(count)
        countries_sel = countries_sec_lockdown

        for count in countries_sel:
            startindex = count.lockdown_fall - days_before_lockdown
            endindex = count.lockdown_fall + days_after_lockdown
            y.append(count.conf[startindex:endindex] / count.population)
            T.append(count.test[startindex:endindex] / count.population)
            tax.append(count.t_shifted[startindex:endindex])
            stringency.append(
                ["Second lockdown", "Pre-lockdown", np.mean(count.stringency[startindex:count.lockdown_fall])])
            stringency.append(["Second lockdown", "During lockdown", np.nanmean(
                count.stringency[count.lockdown_fall:(count.lockdown_fall + days_after_lockdown)])])

    # exclude days between 5 and 10 days after the lockdown.
    lockdownDates = [[5, 10] for count in countries_sel]

    excludeInter = [[1] for count in countries_sel]

    ##############################################################
    # perform regressions to estimate slopes pre and post lockdown based on different testing models
    # key method is infectiousEstimate that estimates the slope of the true number of infectious based on a testing
    # model.
    if useUpSatMod:
        # added up saturating distribution
        # load alphas created in the creation of figure 2
        alphas = np.load('./parameters/alphasOptim.npy', allow_pickle=True)
        alphaMed = [np.nanmedian(np.stack(alph)[:, :, 0], axis=0) if not (alph[0] is None) else None for alph in alphas]
        savMods = np.load('./parameters/modelsOptim.npy', allow_pickle=True)
        savModNames = [mod.name for mod in savMods]
        medAlphaPalpha = [alpha for k, alpha in enumerate(alphaMed) if savMods[k].name == 'Up-saturating'][0]
        pModel = md.PoisAlphaModel(medAlphaPalpha)
        pModel.alphaOptim = False
        # regression for the pre and post lockdown slopes of number of infectious based on pModel as a testing model
        ypredP, paramsP, paramLabels = pModel.infectiousEstimate(tax, lockdownDates, y, T, excludeInter)
    else:
        ypredP, paramsP, paramLabels = pModel.infectiousEstimate(tax, lockdownDates, y, T, excludeInter)

    if useLimModasBaseline:
        baseModel = md.PoissonModel()
    else:
        baseModel = md.BaselineModel()

    testModel = md.BaselineModel()

    # same as ypredP etc above for the baseline model
    ypredB, paramsB, paramLabels = baseModel.infectiousEstimate(tax, lockdownDates, y, T, excludeInter)

    # similar regression to obtain the slopes of the testing numbers (only used in the plot of testing numbers
    tpredB, paramsT, paramLabels = testModel.infectiousEstimate(tax, lockdownDates, T, T, excludeInter)

    # if the baseline is not based on the number of confirmed cases this regression estimates slopes of confirmed cases
    # (only used in the plot of the confirmed cases.
    ypredC, paramsC, paramLabels = testModel.infectiousEstimate(tax, lockdownDates, y, T, excludeInter)

    paramsP = np.array(paramsP)
    paramsB = np.array(paramsB)
    paramsT = np.array(paramsT)
    paramsC = np.array(paramsC)

    labelDict = dict(zip(paramLabels[0], [k for k in range(len(paramLabels[0]))]))

    # naming is suboptimal because confFit actually contains the estimated regression coefficients
    # for the infectious now, conf_panelFit contains estimates for the confirmed
    fullFit = {
        'post': {'slope': paramsP[:, labelDict['pslope2']], 'inter': paramsP[:, labelDict['pintercept2']]},
        'pre': {'slope': paramsP[:, labelDict['pslope0']], 'inter': paramsP[:, labelDict['pintercept0']]}
    }
    grateCorrecVals.append(fullFit)
    confFit = {
        'post': {'slope': paramsB[:, labelDict['pslope2']], 'inter': paramsB[:, labelDict['pintercept2']]},
        'pre': {'slope': paramsB[:, labelDict['pslope0']], 'inter': paramsB[:, labelDict['pintercept0']]}
    }
    grateBaseVals.append(confFit)

    testFit = {
        'post': {'slope': paramsT[:, labelDict['pslope2']], 'inter': paramsT[:, labelDict['pintercept2']]},
        'pre': {'slope': paramsT[:, labelDict['pslope0']], 'inter': paramsT[:, labelDict['pintercept0']]}
    }

    conf_panelFit = {
        'post': {'slope': paramsC[:, labelDict['pslope2']], 'inter': paramsC[:, labelDict['pintercept2']]},
        'pre': {'slope': paramsC[:, labelDict['pslope0']], 'inter': paramsC[:, labelDict['pintercept0']]}
    }

    # pvals contains p-values for wilcoxon tests of, e.g., estimated slopes
    melt_effect, dd, pvals = su.defineFrames(confFit, fullFit, testFit, [baseModel.name, pModel.name, 'Tests'])
    pvalstot.append(pvals)
    conf = y
    test = T
    idxSel = range(len(conf))
    countries = [count.name for count in countries_sel]

    print("P-values of statistical tests performed:")
    print(pvals)

    for i, name in enumerate(countries):
        print(name)
        print("Conf-slope", confFit['pre']['slope'][i])
        print("Corr-slope", fullFit['pre']['slope'][i])
        print("Test slope", testFit['pre']['slope'][i])

    #########################################################################
    # Code below creates the actual plot

    [ax[ld][0].semilogy(tax[k], (conf[k]), '.', label=countries[k]) for k in idxSel]
    [ax[ld][1].semilogy(tax[k], (test[k]), '.', label=countries[k]) for k in idxSel]
    [[ax[ld][l].plot([10, 10], ax[ld][l].get_ylim(), '--') for l in range(2)] for k in range(1)]
    [[ax[ld][l].set_xlabel('days from lockdown onset') for l in range(2)] for k in range(1)]

    x_max = 60
    if ld == 1:
        x_max = 30

    ax[ld][0].semilogy([-20, 0], np.exp(
        ((np.mean(conf_panelFit['pre']['slope'])) * np.array([-20, 0]) + np.mean(conf_panelFit['pre']['inter']))), '-',
                       color='blue', linewidth=4)
    ax[ld][0].semilogy([10, x_max], np.exp(
        (np.mean(conf_panelFit['post']['slope'])) * np.array([10, x_max]) + np.mean(conf_panelFit['post']['inter'])),
                       '-', color='orange', linewidth=4)

    ax[ld][1].semilogy([-20, 0], np.exp(
        ((np.mean(testFit['pre']['slope'])) * np.array([-20, 0]) + np.mean(testFit['pre']['inter']))), '-',
                       color='blue', linewidth=4)
    ax[ld][1].semilogy([10, x_max], np.exp(
        (np.mean(testFit['post']['slope'])) * np.array([10, x_max]) + np.mean(testFit['post']['inter'])), '-',
                       color='orange', linewidth=4)

    ax[ld][0].set_ylabel("Cases per day and $10^6$ inhabitants")
    ax[ld][0].text(-0.1, 1.07, panelLetter[ld][0], transform=ax[ld][0].transAxes,
                   fontsize=16, fontweight='bold', va='top', ha='right')

    ax[ld][1].set_ylabel("Tests per day and $10^6$ inhabitants")
    ax[ld][1].text(-0.1, 1.07, panelLetter[ld][1], transform=ax[ld][1].transAxes,
                   fontsize=16, fontweight='bold', va='top', ha='right')

    ax[ld][0].set_title('New confirmed cases (' + lockName[ld] + ')')
    ax[ld][1].set_title('New tests (' + lockName[ld] + ')')
    moveAx = [-1.43, -.23 + .085]
    ax[ld][0].legend(title='Countries (' + lockName[ld] + ')',
                     loc='upper left', bbox_to_anchor=(0.6 * ld - .2, moveAx[ld]))

    [axlab.spines['right'].set_color('none') for axlab in ax[ld]]
    [axlab.spines['top'].set_color('none') for axlab in ax[ld]]

    ax[ld][2].set_title('Log ratio confirmed/test')
    ax[ld][2].cla()

    # Plot effect boxplots

    dd_no_tests = dd[dd['Variable (log)'] != 'Tests']
    x_label = 'Testing model'
    y_label = 'Growth rate (/day)'
    dd_no_tests.rename(columns={'value': y_label, 'Variable (log)': x_label}, inplace=True)

    test_slopes.append(dd[dd['Variable (log)'] == 'Tests'])
    sns.boxplot(x=x_label, y=y_label, data=dd_no_tests, hue='Period', ax=ax[ld][2])

    height = [[.33, 0.2, .2, 0.03], [.1, .07, .08, .05]]
    center = [-.2, .2, .8, 1.2]
    pu.barplot_annotate_brackets(ax[ld][2], 0, 2, pvals['pr0correcpre'], center, height[ld], maxasterix=2)
    pu.barplot_annotate_brackets(ax[ld][2], 1, 3, pvals['pr0correcpost'], center, height[ld], maxasterix=2)

    heightIndiv = [[.33, 0.03, .15, 0.03], [.1, .04, .08, .03]]
    pu.barplot_annotate_brackets(ax[ld][2], 3, 3, pvals['pcorrec'], center, heightIndiv[ld], maxasterix=2)
    pu.barplot_annotate_brackets(ax[ld][2], 1, 1, pvals['porig'], center, heightIndiv[ld], maxasterix=2)

    ax[ld][2].legend(loc='upper left', bbox_to_anchor=(0.7, 1.02))
    ax[ld][2].set_title('Exp. growth rates (' + lockName[ld] + ')')
    ax[ld][2].text(-0.1, 1.07, panelLetter[ld][2], transform=ax[ld][2].transAxes,
                   fontsize=16, fontweight='bold', va='top', ha='right')

    [axlab.spines['right'].set_color('none') for axlab in ax[ld]]
    [axlab.spines['top'].set_color('none') for axlab in ax[ld]]

    new_column = len(melt_effect) * ['First lockdown']
    if sec_lockdown:
        new_column = len(melt_effect) * ['Second lockdown']
    melt_effect["Lockdown"] = new_column
    effect_frames.append(melt_effect)
    p_vals.append(pvals['peffect'])
    p_valIndiv.append([pvals['peffectorig'], pvals['peffectcorrec']])


ax[ld][2].text(1.55, .067, '*  p<.05\n** p<.005', bbox=dict(facecolor='none', edgecolor='lightgray'))

effect_comb = pd.concat(effect_frames, ignore_index=True)
effect_comb.rename(columns={'value': 'Difference in slopes'}, inplace=True)
sns.boxplot(x='Lockdown', y='Difference in slopes', hue="Variable (log)", data=effect_comb,
            ax=ax[2][2], palette="hls")
ax[2][2].set_title('Estimated lockdown effect')
ax[2][2].legend(loc='upper right')

height = [.42, .22, .22, .09]
center = [-.2, .2, .8, 1.2]
pu.barplot_annotate_brackets(ax[2][2], 0, 1, p_vals[0], center, height, maxasterix=2)
pu.barplot_annotate_brackets(ax[2][2], 2, 3, p_vals[1], center, height, maxasterix=2)

heightIndiv = [.35, .22, .15, .09]

# heightIndiv = [[.33,0.05,.15,0.03],[.1,.05,.08,.05]]
pu.barplot_annotate_brackets(ax[2][2], 0, 0, p_valIndiv[0][0], center, heightIndiv, maxasterix=2)
pu.barplot_annotate_brackets(ax[2][2], 1, 1, p_valIndiv[0][1], center, heightIndiv, maxasterix=2)
pu.barplot_annotate_brackets(ax[2][2], 2, 2, p_valIndiv[1][0], center, heightIndiv, maxasterix=2)
pu.barplot_annotate_brackets(ax[2][2], 3, 3, p_valIndiv[1][1], center, heightIndiv, maxasterix=2)

ax[2][2].text(-0.1, 1.07, panelLetter[2][2], transform=ax[2][2].transAxes,
              fontsize=16, fontweight='bold', va='top', ha='right')

stringency_df = pd.DataFrame(stringency, columns=["Lockdown", "Period", "Average stringency index"])

string_during_first = stringency_df.loc[
    (stringency_df["Lockdown"] == 'First lockdown') & (stringency_df['Period'] == 'During lockdown')]
string_during_second = stringency_df.loc[
    (stringency_df["Lockdown"] == 'Second lockdown') & (stringency_df['Period'] == 'During lockdown')]
p_delta_stringency = kruskal(string_during_first['Average stringency index'],
                             string_during_second['Average stringency index']).pvalue

sns.boxplot(x='Lockdown', y="Average stringency index", hue="Period", data=stringency_df, ax=ax[2][1])
ax[2][1].set_title('Government response stringency')
ax[2][1].legend(loc='lower right')
ax[2][1].text(-0.1, 1.07, panelLetter[2][1], transform=ax[2][1].transAxes,
              fontsize=16, fontweight='bold', va='top', ha='right')
[axlab.spines['right'].set_color('none') for axlab in ax[2]]
[axlab.spines['top'].set_color('none') for axlab in ax[2]]

height = [50, 90, 50, 80]
center = [-.2, .2, .8, 1.2]
pu.barplot_annotate_brackets(ax[2][1], 1, 3, p_delta_stringency, center, height, maxasterix=2)

l, b, w, h = ax[1][0].get_position().bounds

ax[1][0].set_position([l, b - .02, w, h])

l, b, w, h = ax[1][1].get_position().bounds

ax[1][1].set_position([l, b - .02, w, h])

l, b, w, h = ax[1][2].get_position().bounds

ax[1][2].set_position([l, b - .02, w, h])

l, b, w, h = ax[2][1].get_position().bounds

ax[2][1].set_position([l, b - .04, w, h])

l, b, w, h = ax[2][2].get_position().bounds

ax[2][2].set_position([l, b - .04, w, h])

ax[2][0].set_visible(False)
if useLimModasBaseline:
    fig.savefig("./figures/Fig3Lim.pdf", bbox_inches='tight')

elif useUpSatMod:
    fig.savefig("./figures/Fig3.pdf", bbox_inches='tight')
else:
    fig.savefig("./figures/Fig3Org.pdf", bbox_inches='tight')

df_test_first = test_slopes[0]
df_test_first['Lockdown'] = 'First lockdown'
df_test_second = test_slopes[1]
df_test_second['Lockdown'] = 'Second lockdown'
df_test = pd.concat([df_test_first, df_test_second], axis=0, ignore_index=True)
df_test = df_test.drop(columns=['Variable (log)'])
df_test.rename(columns={'value': 'growth-rate (/day)'}, inplace=True)
tfig, testax = plt.subplots(1, 1, figsize=[6, 5])
sns.boxplot(x='Lockdown', y='growth-rate (/day)', data=df_test, hue='Period', ax=testax)
testax.set_title('Slopes of the testing number')
testax.legend(loc='upper right')
p_delta_first = pvalstot[0]['testchange']
p_delta_second = pvalstot[1]['testchange']
p_pre_first = stest(df_test_first[df_test_first['Period'] == 'Pre-lockdown']['value']).pvalue
p_dur_first = stest(df_test_first[df_test_first['Period'] == 'During lockdown']['value']).pvalue
p_pre_second = stest(df_test_second[df_test_second['Period'] == 'Pre-lockdown']['value']).pvalue
p_dur_second = stest(df_test_second[df_test_second['Period'] == 'During lockdown']['value']).pvalue

height = [.34, .1, .1, .05]
center = [-.2, .2, .8, 1.2]
pu.barplot_annotate_brackets(testax, 0, 1, p_delta_first, center, height, maxasterix=2)
pu.barplot_annotate_brackets(testax, 2, 3, p_delta_second, center, height, maxasterix=2)

heightIndiv = [.27, .015, .015, .0]

# heightIndiv = [[.33,0.05,.15,0.03],[.1,.05,.08,.05]]
pu.barplot_annotate_brackets(testax, 0, 0, p_pre_first, center, heightIndiv, maxasterix=2)
pu.barplot_annotate_brackets(testax, 1, 1, p_dur_first, center, heightIndiv, maxasterix=2)
pu.barplot_annotate_brackets(testax, 2, 2, p_pre_second, center, heightIndiv, maxasterix=2)
pu.barplot_annotate_brackets(testax, 3, 3, p_dur_second, center, heightIndiv, maxasterix=2)

testax.text(1.3, .24, '*  p<.05\n** p<.005', bbox=dict(facecolor='none', edgecolor='lightgray'))
testax.spines['right'].set_color('none')
testax.spines['top'].set_color('none')

tfig.savefig("./figures/sFig3.pdf", bbox_inches='tight')


# print median results for growth rates
grateBaseVals[0]['pre']['slope']
grateCorrecVals[0]['pre']['slope']

# print pvalue dictionaries of the two lockdowns
pvalstot


########## Generate the same figure for the US ###############################
panelLetter = [['A', 'B'], ['C', 'D']]
figus, axus = plt.subplots(2, 2, figsize=[12, 12])
effect_frames = []
p_vals = []
stringency = []
test_slopes = []
pvalstotUS = []
p_valIndiv = []
ld = 0

all_data = preprocess_dataset("USstates", local=True)

min_death = 1
min_index = -7
set_selection_variable(all_data, min_death, "death")
set_selection_variable(all_data, min_index, "time", False)

countries_sel = get_selected_countries(all_data)
countries_sel = sorted(countries_sel, key=lambda c: c.name)

############################## Comparison of lockdown effect and growth rates for Poisson and baseline testing models ####################################

pModel = md.PoissonModel()
baseModel = md.BaselineModel()

# remove post lockdown samples while building the lists
y = []
T = []
tax = []
for count in countries_sel:
    y.append(count.conf[:50])
    T.append(count.test[:50])
    tax.append(count.t[:50])

lockdownDates = [[5, 10] for count in countries_sel]

excludeInter = [[1] for count in countries_sel]

if useUpSatMod is True:
    # added up saturating distribution
    alphas = np.load('./parameters/alphasOptimUS.npy', allow_pickle=True)
    alphaMed = [np.nanmedian(np.stack(alph)[:, :, 0], axis=0) if not (alph[0] is None) else None for alph in alphas]
    savMods = np.load('./parameters/modelsOptim.npy', allow_pickle=True)
    savModNames = [mod.name for mod in savMods]
    medAlphaPalpha = [alpha for k, alpha in enumerate(alphaMed) if savMods[k].name == 'Up-saturating'][0]
    pModel = md.PoisAlphaModel(medAlphaPalpha)
    pModel.alphaOptim = False
    ypredP, paramsP, paramLabels = pModel.infectiousEstimate(tax, lockdownDates, \
                                                             y, T, excludeInter)
else:
    ypredP, paramsP, paramLabels = pModel.infectiousEstimate(tax, lockdownDates, \
                                                             y, T, excludeInter)

if useLimModasBaseline:
    baseModel = md.PoissonModel()
else:
    baseModel = md.BaselineModel()

testModel = md.BaselineModel()
ypredB, paramsB, paramLabels = baseModel.infectiousEstimate(tax, lockdownDates, y, T, excludeInter)

tpredB, paramsT, paramLabels = testModel.infectiousEstimate(tax, lockdownDates, T, T, excludeInter)

paramsP = np.array(paramsP)
paramsB = np.array(paramsB)
paramsT = np.array(paramsT)

labelDict = dict(zip(paramLabels[0], [k for k in range(len(paramLabels[0]))]))

fullFit = {
    'post': {'slope': paramsP[:, labelDict['pslope2']], 'inter': paramsP[:, labelDict['pintercept2']]},
    'pre': {'slope': paramsP[:, labelDict['pslope0']], 'inter': paramsP[:, labelDict['pintercept0']]}
}
confFit = {
    'post': {'slope': paramsB[:, labelDict['pslope2']], 'inter': paramsB[:, labelDict['pintercept2']]},
    'pre': {'slope': paramsB[:, labelDict['pslope0']], 'inter': paramsB[:, labelDict['pintercept0']]}
}
testFit = {
    'post': {'slope': paramsT[:, labelDict['pslope2']], 'inter': paramsT[:, labelDict['pintercept2']]},
    'pre': {'slope': paramsT[:, labelDict['pslope0']], 'inter': paramsT[:, labelDict['pintercept0']]}
}

melt_effect, dd, pvals = su.defineFrames(confFit, fullFit, testFit, [baseModel.name, pModel.name, 'Tests'])
pvalstotUS.append(pvals)
conf = y
test = T
idxSel = range(len(conf))
countries = [count.name for count in countries_sel]

print("P-values US:")
print(pvals)

[axus[ld][0].semilogy(tax[k], (conf[k]), '.', label=countries[k]) for k in idxSel]
[axus[ld][1].semilogy(tax[k], (test[k]), '.', label=countries[k]) for k in idxSel]
[[axus[ld][l].plot([10, 10], axus[ld][l].get_ylim(), '--') for l in range(2)] for k in range(1)]
[[axus[ld][l].set_xlabel('days from lockdown onset') for l in range(2)] for k in range(1)]

x_max = 40

axus[ld][0].semilogy([-20, 0], np.exp(
    ((np.mean(confFit['pre']['slope'])) * np.array([-20, 0]) + np.mean(confFit['pre']['inter']))), '-', color='blue',
                     linewidth=4)
axus[ld][0].semilogy([10, x_max], np.exp(
    (np.mean(confFit['post']['slope'])) * np.array([10, x_max]) + np.mean(confFit['post']['inter'])), '-',
                     color='orange', linewidth=4)

axus[ld][1].semilogy([-20, 0], np.exp(
    ((np.mean(testFit['pre']['slope'])) * np.array([-20, 0]) + np.mean(testFit['pre']['inter']))), '-', color='blue',
                     linewidth=4)
axus[ld][1].semilogy([10, x_max], np.exp(
    (np.mean(testFit['post']['slope'])) * np.array([10, x_max]) + np.mean(testFit['post']['inter'])), '-',
                     color='orange', linewidth=4)

axus[ld][0].set_ylabel("Cases per day and $10^6$ inhabitants")
axus[ld][0].text(-0.1, 1.07, panelLetter[ld][0], transform=axus[ld][0].transAxes,
                 fontsize=16, fontweight='bold', va='top', ha='right')

axus[ld][1].set_ylabel("Tests per day and $10^6$ inhabitants")
axus[ld][1].text(-0.1, 1.07, panelLetter[ld][1], transform=axus[ld][1].transAxes,
                 fontsize=16, fontweight='bold', va='top', ha='right')

axus[ld][0].set_title('New confirmed cases (' + lockName[ld] + ')')
axus[ld][1].set_title('New tests (' + lockName[ld] + ')')
moveAx = [-1.43, -.23]

[axlab.spines['right'].set_color('none') for axlab in axus[ld]]
[axlab.spines['top'].set_color('none') for axlab in axus[ld]]

# Plot effect boxplots

dd_no_tests = dd[dd['Variable (log)'] != 'Tests']
x_label = 'Testing model'
y_label = 'Growth rate (/day)'
dd_no_tests.rename(columns={'value': y_label, 'Variable (log)': x_label}, inplace=True)

test_slopes.append(dd[dd['Variable (log)'] == 'Tests'])
sns.boxplot(x=x_label, y=y_label, data=dd_no_tests, hue='Period', ax=axus[1][0])
height = [[.33, 0.15, .15, 0.03], [.1, .07, .08, .05]]
center = [-.2, .2, .8, 1.2]
pu.barplot_annotate_brackets(axus[1][0], 0, 2, pvals['pr0correcpre'], center, height[ld], maxasterix=2)
pu.barplot_annotate_brackets(axus[1][0], 1, 3, pvals['pr0correcpost'], center, height[ld], maxasterix=2)

heightIndiv = [[.33, 0.07, .15, 0.03], [.1, .03, .08, .03]]
pu.barplot_annotate_brackets(axus[1][0], 3, 3, pvals['pcorrec'], center, heightIndiv[ld], maxasterix=2)
pu.barplot_annotate_brackets(axus[1][0], 1, 1, pvals['porig'], center, heightIndiv[ld], maxasterix=2)

axus[1][0].set_title('Exp. growth rates (' + lockName[ld] + ')')

axus[1][0].text(-0.1, 1.07, panelLetter[1][0], transform=axus[1][0].transAxes,
                fontsize=16, fontweight='bold', va='top', ha='right')

[axlab.spines['right'].set_color('none') for axlab in axus[ld]]
[axlab.spines['top'].set_color('none') for axlab in axus[ld]]
axus[1][0].spines['right'].set_color('none')
axus[1][0].spines['top'].set_color('none')

effect_frame = melt_effect
p_vals.append(pvals['peffect'])
p_valIndiv.append([pvals['peffectorig'], pvals['peffectcorrec']])

axus[1][1].text(1.07, .35, '*  p<.05\n** p<.005', bbox=dict(facecolor='none', edgecolor='lightgray'))

effect_frame.rename(columns={'value': 'Difference in slopes', 'Variable (log)': 'Testing model'}, inplace=True)
sns.boxplot(x='Testing model', y='Difference in slopes', data=effect_frame,
            ax=axus[1][1], palette="hls")
axus[1][1].set_title('Estimated 1st lockdown effect')
axus[1][1].legend(loc='upper right')

# use new statistic plot
height = [.36, .2]
center = [0., 1.]
pu.barplot_annotate_brackets(axus[1][1], 0, 1, p_vals[0], center, height, maxasterix=2)

heightIndiv = [.28, .16]
pu.barplot_annotate_brackets(axus[1][1], 0, 0, p_valIndiv[0][0], center, heightIndiv, maxasterix=2)
pu.barplot_annotate_brackets(axus[1][1], 1, 1, p_valIndiv[0][1], center, heightIndiv, maxasterix=2)

axus[1][1].text(-0.1, 1.07, panelLetter[1][1], transform=axus[1][1].transAxes,
                fontsize=16, fontweight='bold', va='top', ha='right')
axus[1][1].spines['right'].set_color('none')
axus[1][1].spines['top'].set_color('none')

figus.savefig("./figures/sFig3US.pdf", bbox_inches='tight')
