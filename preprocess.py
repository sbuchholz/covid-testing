# -*- coding: utf-8 -*-
""" Script to download and extract the testing numbers, case number and death numbers for countries or states of the US.
    Returns a dictionary with country names as keys and CountryData objects as values which collect all relevant
    country data."""

import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from scipy.interpolate import interp1d


# Testing data is taken from an earlier version of the Owid data which was later restructured
TESTING_DATA_FRANCE = [2.9981115436091678e-36, 1.1122026694033919e-35, 5.238115797835392e-35, 2.4669835693030944e-34,
                       1.1618696810266325e-33, 5.472031400964084e-33, 2.577150272712147e-32, 1.2137546445676441e-31,
                       5.716392842157256e-31, 2.6922366288869966e-30, 1.2679566058628954e-29, 5.971666595354351e-29,
                       2.8124623320055694e-28, 1.3245790337832835e-27, 6.238339965559917e-27, 2.9380568870056467e-26,
                       1.3837300177510474e-25, 6.516922019085584e-25, 3.0692600476983734e-24, 1.4455224740772866e-23,
                       6.807944555331755e-23, 3.206322274446571e-22, 1.5100743615135475e-21, 7.111963121967036e-21,
                       3.349505212281252e-20, 1.5775089064292358e-19, 7.429558075440923e-19, 3.499082190368953e-18,
                       1.6479548380447194e-17, 7.761335688855796e-17, 3.6553387437837005e-16, 1.721546634169099e-15,
                       8.107929309312536e-15, 3.8185731585794386e-14, 1.7984247779116094e-13, 8.470000566938516e-13,
                       3.9890970412033085e-12, 1.878736025857035e-11, 8.848240637907341e-11, 4.167235913337009e-10,
                       1.9626336882167868e-09, 9.243371563859711e-09, 4.353329833301654e-08, 2.0502779214904602e-07,
                       9.6561476302454e-07, 4.547734045212353e-06, 2.141836034196778e-05, 0.000100873568062171,
                       0.00047508196571216104, 0.002237482806257271, 0.010537822248824567, 0.049629743494464085,
                       0.2337400822642502, 1.1008403874380814, 5.1846031150309635, 24.41780821917808, 115.0,
                       266.0000000000001, 439.0000000000001, 465.0, 586.0, 548.9999999999989, 436.00000000000045,
                       707.0000000000014, 1209.0, 1243.0000000000036, 1648.9999999999973, 1799.999999999989,
                       1576.000000000009, 1595.999999999989, 2351.000000000022, 2943.146610234515, 3519.9286031319934,
                       4209.7452189647265, 5034.748373255989, 6021.431194412795, 4656.880065271827, 5247.037954142368,
                       5911.985472317196, 6661.200572657646, 7505.362331647128, 8456.503165589762, 9528.180336889258,
                       10735.66919500487, 12096.180906479858, 11904.105793884795, 13221.753449325057,
                       14685.249551843503, 16310.737847779354, 18116.14901059633, 20121.398433167924,
                       22348.605913403124, 13112.550475694268, 13879.26580731658, 14690.81241725001, 15549.811674120661,
                       16459.038222874864, 17421.42894713761, 18440.092455606034, 16043.114453617309,
                       16814.163357241545, 17622.26967970276, 18469.21443941188, 19356.864252386615, 20287.175446173525,
                       21262.1983714666, 16835.571952578903, 17446.871742017916, 18080.36783304921, 18736.866173612827,
                       19417.20197562303, 20122.240777549567, 20852.879545568256, 16966.583910952206, 17450.26539441268,
                       17947.73561570444, 18459.387662630295, 18985.625829091645, 19526.86593456217, 20083.53565264633,
                       14347.573911181418, 14631.675848508603, 14921.403399705305, 15216.867959758383,
                       15518.183129429584, 15825.46475893422, 16138.830992483301, 14452.442957249936,
                       14703.741851864732, 14959.410328467959, 15219.524365295656, 15484.16126169276,
                       15753.399661083706, 16027.319574344554, 40036.99999999942, 43348.000000000815,
                       48659.999999999185, 16735.0, 6692.000000001397, 52167.99999999977, 53788.99999999977, 54151.0,
                       9768.999999999534, 51352.00000000023, 19267.0000000007, 7221.999999998137, 52560.000000002095,
                       54662.99999999814, 47311.00000000163, 45203.9999999993, 50142.000000001164, 20347.0,
                       8570.999999997672, 10127.000000000233, 49649.99999999977, 45558.00000000093, 41940.999999999534,
                       47114.000000002095, 19752.999999997206, 7539.000000002561, 45768.0, 44430.9999999993,
                       40947.999999998836, 39100.000000002095, 44932.99999999814, 19215.000000001863, 6641.000000000466,
                       44272.999999999534, 43584.000000000466, 40515.999999997206, 38357.00000000233, 46354.99999999814,
                       21852.000000000466, 6581.999999999534, 49046.99999999907, 48881.00000000186, 46057.99999999907,
                       40589.99999999814, 51978.00000000326, 24861.99999999907, 8418.000000001863, 62639.000000000466,
                       60137.999999999534, 57766.999999999534, 55461.000000000466, 64907.9999999986, 29321.99999999907,
                       8785.999999999069, 67651.99999999953, 66737.99999999953, 65770.99999999953, 69348.00000000047,
                       78054.00000000326, 32841.00000000186, 8626.99999999674, 81426.9999999986, 12562.999999999534,
                       81071.00000000233, 85369.00000000186, 95072.0, 40950.99999999907, 12443.000000001397, 93791.0]


class CountryData:
    def __init__(self, name, data, population, lockdown, end_lockdown, death_rate, min_cases=10):
        self.name = name
        self.US = True if len(name) < 3 else False
        # population in million
        self.population = population
        self.lockdown = datetime.strptime(lockdown, '%Y-%m-%d')
        date = datetime.strptime('2020-05-01', '%Y-%m-%d')
        # start time series when total number of cases reaches min_cases
        self.min_cases = min_cases
        self.death_rate = death_rate
        self.attributes = dict()
        self.attributes["sel"] = True
        t_max = -1
        if name == "France":
            test = np.array(data['new_tests'])
            test[0:203] = np.array(TESTING_DATA_FRANCE)
            self.total_test = np.nansum(test)
            if True in np.isnan(test):
                t_max = list(np.isnan(test)).index(True)
        else:
            test, self.total_test = extract_time_series(data, 'total_tests')
        death, self.total_death = extract_time_series(data, 'total_deaths')
        conf, self.total_conf = extract_time_series(data, 'total_cases')
        dates = data['date']
        t = np.array([(datetime.strptime(d, '%Y-%m-%d') - self.lockdown).days for d in dates])
        t_min = 0
        if np.amax(conf) <= self.min_cases:
            self.attributes["sel"] = False
        else:
            t_min = np.nonzero(conf > self.min_cases)[0][0]
        self.conf = conf[t_min:t_max]
        self.test = test[t_min:t_max]
        self.death = death[t_min:t_max]
        self.test = np.maximum(self.test, self.conf)
        self.t = t[t_min:t_max]
        dates = dates[t_min:t_max]
        if not self.US:
            self.stringency = np.array(data["stringency_index"])[t_min:]
        self.dates = [datetime.strptime(d, '%Y-%m-%d') for d in dates]
        self.end_lockdown = datetime.strptime(end_lockdown, '%Y-%m-%d') if end_lockdown is not None else date
        self.index_end_lockdown = self.t[-1]
        if end_lockdown is not None:
            self.index_end_lockdown = (self.end_lockdown - self.lockdown).days - self.t[0]

    def get_index(self, startdate):
        index_from_lockdown = (startdate - self.lockdown).days
        index = np.nonzero(self.t == index_from_lockdown)
        if index[0].size > 0:
            return index[0].item()
        else:
            return 0

    def get_date(self, index):
        return self.lockdown + timedelta(int(index))

    def get_lockdown_index(self, earliest_date, threshold, min_data_after=0):
        index = (earliest_date - self.lockdown).days - self.t[0]
        if index < 0:
            print("Occurs for huge number of minimal cases in preprocess...")
            return None
        if self.stringency[index] > threshold:
            self.lockdown_fall = None
            print("No second lockdown for {} because measures were not lifted below threshold".format(self.name))
            return None
        while index < self.stringency.size:
            if self.stringency[index] > threshold:
                if len(self.t) - index < min_data_after:
                    self.lockdown_fall = None
                    print("No fall lockdown for {}, only {} days after lockdown".format(self.name, len(self.t) - index))
                    return None
                self.lockdown_fall = index
                self.t_shifted = self.t - index - self.t[0]
                return index
            index += 1
        print("No second lockdown for {} because measures not sufficiently strict".format(self.name))
        self.lockdown_fall = None
        return None

    def rolling_average(self, a, l=7):
        filter = np.ones(l) / l
        return np.convolve(a, filter)[: -(l - 1)]

    def get_data_as_tuple(self, normalized=False):
        if normalized:
            return self.conf / self.population, self.test / self.population, self.death / self.population
        return self.conf, self.test, self.death


def get_selected_countries(all_data):
    res = []
    for x in all_data.values():
        if x.attributes["sel"]:
            res.append(x)
    return res


def set_selection_variable(all_data, condition_value, column, bigger_than=True):
    for x in all_data.values():
        if "sel" not in x.attributes:
            x.attributes["sel"] = True
        if column == "test":
            value = x.total_test
        elif column == "death":
            value = x.total_death
        elif column == "conf":
            value = x.total_conf
        elif column == "time":
            value = x.t[0]
        else:
            raise ValueError('No such column implemented in country data!')
        if bigger_than:
            x.attributes["sel"] = x.attributes["sel"] and (value >= condition_value)
        else:
            x.attributes["sel"] = x.attributes["sel"] and (value <= condition_value)


def extract_time_series(data, column_name):
    """Code to extract times series from a data frame using linear interpolation of the log transformed series"""
    time_series = np.array(data[column_name])
    max_value = np.nanmax(time_series)
    t = np.arange(time_series.size)
    # avoid zero derivative if not increasing
    time_series[np.concatenate([[False], np.diff(time_series)]) < 1] = np.nan
    # interpolate + extrapolate testing in log scale
    idx_dates = np.squeeze(np.logical_not(np.isnan(time_series)))
    assert (True in idx_dates)
    log_interp = interp1d(t[idx_dates], np.log(time_series[idx_dates]), fill_value="extrapolate")
    interp = np.exp(log_interp(t))
    diff = np.diff(interp, prepend=0)
    # Erase negative values! Slightly inappropriate
    diff = np.where(diff > 0, diff, 1)
    return diff, max_value


def check_data_valid(data):
    # Interpolation also fails if only one value, so require at least two values
    time_series = np.array(data['total_deaths'])
    if np.unique(time_series[~np.isnan(time_series)]).size < 2:
        return False
    time_series = np.array(data['total_tests'])
    if np.unique(time_series[~np.isnan(time_series)]).size < 2:
        return False
    time_series = np.array(data['total_cases'])
    if np.unique(time_series[~np.isnan(time_series)]).size < 2:
        return False
    return True


def preproc(file_name, countries, lockdown, end_lockdown, population, death_rate=None, min_cases=10, data_until=None):
    aggregated_data = dict()
    print("Preprocessing data....")
    data = pd.read_csv(file_name)
    # Rename columns for US
    if len(countries[0]) < 3:
        # death contains also cases without positive test but high probability of Covid related death. Available for all
        # states.
        data.rename(
            columns={'state': 'location', 'totalTestResults': 'total_tests', 'death': 'total_deaths',
                     'positive': 'total_cases'}
            , inplace=True)
        data = data.reindex(index=data.index[::-1])
    for k, country in enumerate(countries):
        print(country)
        country_data = data[(data['location'] == country)]
        if data_until is not None:
            country_data = country_data[data['date'] <= data_until]
        if check_data_valid(country_data) or country == "France":
            death_rate_country = None if death_rate is None else death_rate[k]
            data_object = CountryData(country, country_data, population=population[k], lockdown=lockdown[k],
                                      end_lockdown=end_lockdown[k], death_rate=death_rate_country, min_cases=min_cases)
            if data_object.attributes["sel"]:
                aggregated_data[country] = data_object
            else:
                print("Number of cases not sufficient for ", country)
        else:
            print("Data for country ", country, " is not valid.")
    return aggregated_data


def preprocess_dataset(dataset, local=False):
    # Local allows to use the version saved locally after downloading once
    # To ensure reproducibility we provide the option to consider only data up to a given date
    # Format must be '%Y-%m-%d'
    data_until = '2021-01-29'
    if dataset == "Countries":
        countries = ['France', 'Germany', 'Denmark', 'Switzerland', 'Canada', 'Belgium', 'Austria', 'Czech Republic',
                     'Finland', 'United Kingdom', 'Hungary', 'Italy', 'South Korea', 'Lithuania', 'Luxembourg',
                     'Mexico', 'Malaysia', 'Norway', 'New Zealand', 'Panama', 'Peru', 'Taiwan', 'Thailand',
                     'United States', 'United Arab Emirates', 'Argentina', 'Bangladesh', 'Colombia', 'Costa Rica',
                     'Greece', 'Croatia', 'Morocco', 'Pakistan', 'Portugal', 'Romania', 'Russia', 'South Africa']
        # using national recommendation or national lockdown date (the earliest of them) from
        # https://www.bbc.com/news/world-52103747
        # lockdown dates for Tunesia, Morocco, and United Arab Emirates from wikipedia
        lockdown = ['2020-03-08', '2020-03-18', '2020-03-13', '2020-03-13', '2020-03-13', '2020-03-12', '2020-03-13',
                    '2020-03-16', '2020-03-16', '2020-03-22', '2020-03-11', '2020-03-12', '2020-02-22', '2020-03-13',
                    '2020-03-15', '2020-03-18', '2020-03-13', '2020-03-12', '2020-03-21', '2020-03-13', '2020-03-16',
                    '2020-03-25', '2020-03-20', '2020-03-16', '2020-03-26', '2020-03-20', '2020-03-23', '2020-03-24',
                    '2020-03-15', '2020-03-11', '2020-03-19', '2020-03-19', '2020-03-23', '2020-03-12', '2020-03-12',
                    '2020-03-05', '2020-03-15']
        population = [65.273512, 83.783945, 5.792203, 8.654618, 37.742157, 11.589616, 9.0064, 10.708982, 5.540718,
                      67.886004, 9.66035, 60.461828, 51.269183, 2.722291, 0.625976, 128.932753, 32.365998, 5.421242,
                      4.822233, 4.314768, 32.971846, 23.816775, 69.799978, 331.002647, 9.8904, 45.195777, 164.689383,
                      50.882884, 5.094114, 10.423056, 4.105268, 36.910558, 220.892331, 10.196707, 19.237682, 145.93446,
                      59.30869]
        # determined by the point in time when mobility reduction is 80% of maximal mobility reduction
        end_lockdown = ['2020-05-12', '2020-04-22', '2020-04-20', '2020-04-30', '2020-05-10', '2020-05-08',
                        '2020-04-21', '2020-04-20', '2020-04-23', '2020-05-29', '2020-04-20', '2020-05-07',
                        '2020-03-12', '2020-04-23', '2020-05-08', '2020-06-04', '2020-05-10', '2020-04-18',
                        '2020-05-04', '2020-06-01', '2020-06-04', '2020-04-10', '2020-05-12', '2020-05-04',
                        '2020-05-12', '2020-05-08', '2020-05-11', '2020-05-09', '2020-04-17', '2020-04-30',
                        '2020-04-26', '2020-06-02', '2020-04-21', '2020-05-08', '2020-04-30', '2020-04-27',
                        '2020-05-05']
        filename = "https://covid.ourworldindata.org/data/owid-covid-data.csv"
        if local:
            filename = './datasets/owid-covid-data_updated.csv'
        return preproc(filename, countries, lockdown, end_lockdown, population, data_until=data_until)

    elif dataset == "USstates":
        filename = "https://covidtracking.com/data/download/all-states-history.csv"
        if local:
            filename = './datasets/US_data_updated.csv'
        countries = ['AL', 'AK', 'AZ', 'CA', 'CO', 'CT', 'DE', 'DC', 'FL', 'GA', 'HI', 'ID', 'IL', 'IN', 'KS', 'KY',
                     'LA',
                     'ME', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'OH', 'OK',
                     'OR',
                     'PA', 'PR', 'RI', 'SC', 'TN', 'TX', 'UT', 'VT', 'VA', 'WA', 'WV', 'WI', 'WY']
        # using data from Wikipedia
        lockdown = ['2020-04-04', '2020-03-28', '2020-03-31', '2020-03-19', '2020-03-26', '2020-03-23', '2020-03-24',
                    '2020-04-01', '2020-04-03', '2020-04-03', '2020-03-25', '2020-03-25', '2020-03-21', '2020-03-25',
                    '2020-03-30', '2020-03-26', '2020-03-23', '2020-04-02', '2020-03-30', '2020-03-24', '2020-03-24',
                    '2020-03-28', '2020-04-03', '2020-04-06', '2020-03-28', '2020-04-01', '2020-03-28', '2020-03-21',
                    '2020-03-24', '2020-03-22', '2020-03-30', '2020-03-24', '2020-04-07', '2020-03-23', '2020-04-01',
                    '2020-03-15', '2020-03-28', '2020-04-07', '2020-04-01', '2020-04-02', '2020-04-02', '2020-03-25',
                    '2020-03-30', '2020-03-23', '2020-03-24', '2020-03-25', '2020-03-28']
        population = [4.903185, 0.731545, 7.278717, 39.512223, 5.758736, 3.565287, 0.973764, 0.705749, 21.477737,
                      10.617423, 1.415872, 1.787065, 12.671821, 6.732219, 2.913314, 4.467673, 4.648794, 1.344212,
                      6.04568, 6.892503, 9.986857, 5.639632, 2.976149, 6.137428, 1.068778, 3.080156, 1.359711,
                      8.88219, 2.096829, 19.453561, 10.488084, 11.6891, 3.956971, 4.217737, 12.801989, 3.193694,
                      1.059361, 5.148714, 6.829174, 28.995881, 3.205958, 0.623989, 8.535519, 7.614893, 1.792147,
                      5.822434, 0.578759]
        end_lockdown = len(countries) * [None]
        return preproc(filename, countries, lockdown, end_lockdown, population, min_cases=10, data_until=data_until)

    elif dataset == "Countries_all":
        countries = ['Argentina', 'Belgium', 'Bangladesh', 'Bolivia', 'Canada', 'Switzerland', 'Chile', 'Colombia',
                     'Germany', 'Ecuador', 'Spain', 'United Kingdom', 'Indonesia', 'India', 'Ireland', 'Iran', 'Italy',
                     'Japan', 'Kazakhstan', 'Mexico', 'Netherlands', 'Pakistan', 'Panama', 'Peru', 'Philippines',
                     'Poland', 'Portugal', 'Romania', 'Russia', 'Saudi Arabia', 'Turkey', 'Ukraine', 'United States',
                     'South Africa']
        # using Wikipedia data, if no lockdown happened or no data is available we put 01.01.2020 to have some
        # data for the plots should give no result for pre-lockdown regression and wrong results for post
        lockdown = ['2020-03-19', '2020-03-18', '2020-03-26', '2020-03-22', '2020-01-01', '2020-01-01', '2020-01-01',
                    '2020-03-25', '2020-03-23', '2020-03-16', '2020-03-14', '2020-03-23', '2020-01-01', '2020-03-25',
                    '2020-03-12', '2020-03-14', '2020-03-09', '2020-01-01', '2020-01-01', '2020-03-23', '2020-01-01',
                    '2020-03-24', '2020-03-25', '2020-03-16', '2020-01-01', '2020-03-13', '2020-03-19', '2020-03-25',
                    '2020-03-28', '2020-01-01', '2020-04-23', '2020-03-17', '2020-03-19', '2020-03-26']
        # population in millions
        population = [45.195777, 11.589616, 164.689383, 11.673029, 37.742157, 8.654618, 19.116209, 50.882884, 83.783945,
                      17.64306, 46.754783, 67.886004, 273.523621, 1380.004385, 4.937796, 83.992953, 60.461828,
                      126.476458, 18.776707, 128.932753, 17.134873, 220.892331, 4.314768, 32.971846, 109.581085,
                      37.846605, 10.196707, 19.237682, 145.93446, 34.813867, 84.339067, 43.733759, 331.002647, 59.30869]
        # Those countries are not suitable for lockdown analysis, use 'Countries' dataset instead
        end_lockdown = ['2020-05-08', '2020-05-08', '2020-05-11', '2020-05-27', '2020-05-10', '2020-04-30',
                        '2020-05-08', '2020-05-09', '2020-04-22', '2020-05-16', '2020-05-08', '2020-05-29',
                        '2020-05-20', '2020-05-19', '2020-05-19', 'NA-NA-NA', '2020-05-07', '2020-05-13', '2020-05-07',
                        '2020-06-04', '2020-05-08', '2020-04-21', '2020-06-01', '2020-06-04', '2020-05-30',
                        '2020-04-23', '2020-05-08', '2020-04-30', '2020-04-27', '2020-06-01', '2020-05-10',
                        '2020-04-30', '2020-05-04', '2020-05-05']

        end_lockdown = len(countries) * [None]
        death_rate = [0.8634057531525567, 1.5049862627027508, 0.4330670116725132, 0.5819114430367643, 1.355192410439075,
                      1.5193024983887213, 0.9284628427111258, 0.6822519187395117, 1.7944261428606638,
                      0.5830237577835139, 1.638727908757485, 1.4529228435658108, 0.47292132945256654,
                      0.4769331387450628, 1.0864965968622438, 0.4905828940196923, 1.8925868243679305, 2.260243634194753,
                      0.5997846571286435, 0.5880661834623201, 1.5036369099438323, 0.3330122592621832,
                      0.6715911863627431, 0.6647222269569013, 0.40718322199492735, 1.347330113229443,
                      1.8079151082795655, 1.4212645109738269, 1.1367477802706778, 0.3024863560833389, 0.679001259048787,
                      1.261364873986707, 1.235387029216114, 0.40344565745761723]
        filename = "https://covid.ourworldindata.org/data/owid-covid-data.csv"
        if local:
            filename = './datasets/owid-covid-data_updated.csv'
        return preproc(filename, countries, lockdown, end_lockdown, population, death_rate, data_until=data_until)

    elif dataset == "World":
        filename = "https://covid.ourworldindata.org/data/owid-covid-data.csv"
        if local:
            filename = './datasets/owid-covid-data_updated.csv'
        data = pd.read_csv(filename)
        countries = list(data["location"].unique())
        countries.remove("Fiji")
        countries.remove("Jordan")
        # remove chine because of likely lag in reporting at the beginning
        countries.remove("China")
        print(len(countries), "countries investigated in total")
        population = []
        for c in countries:
            country_data = data.loc[data["location"] == c]
            population.append(country_data.iloc[0]["population"] / 1.e6)
        # using Wikipedia data, if no lockdown happened or no data is available we put 01.01.2020 to have some
        # data for the plots should give no result for pre-lockdown regression and wrong results for post
        lockdown = ['2020-03-19', '2020-03-18', '2020-03-26', '2020-03-22', '2020-01-01', '2020-01-01', '2020-01-01',
                    '2020-03-25', '2020-03-23', '2020-03-16', '2020-03-14', '2020-03-23', '2020-01-01', '2020-03-25',
                    '2020-03-12', '2020-03-14', '2020-03-09', '2020-01-01', '2020-01-01', '2020-03-23', '2020-01-01',
                    '2020-03-24', '2020-03-25', '2020-03-16', '2020-01-01', '2020-03-13', '2020-03-19', '2020-03-25',
                    '2020-03-28', '2020-01-01', '2020-04-23', '2020-03-17', '2020-03-19', '2020-03-26']
        lockdown = len(countries) * ['2020-01-01']
        end_lockdown = len(countries) * [None]
        death_rate = len(countries) * [0.]
        return preproc(filename, countries, lockdown, end_lockdown, population, death_rate, min_cases=20,
                       data_until=data_until)
    else:
        print('unknown dataset')
        return None


def attach_attribute(data_objects, file_name, country_column_name, attribute_column_name, name, default_value):
    data = pd.read_csv(file_name)
    data = data[[country_column_name, attribute_column_name]]
    print("Extracting attribute", name)
    for country_object in data_objects.values():
        data_country = data[data[country_column_name] == country_object.name]
        if len(data_country) > 0:
            if pd.notnull(data_country.iloc[0, 1]):
                country_object.attributes[name] = data_country.iloc[0, 1]
            else:
                print("No data for country ", country_object.name)
                country_object.attributes[name] = default_value
        else:
            print("No data for country ", country_object.name)
            country_object.attributes[name] = default_value


def main():
    all_data = preprocess_dataset("Countries")
    print(all_data['Germany'].conf)
    print(all_data['Germany'].test)
    print(all_data['Germany'].t)
    preprocess_dataset("Countries_all")
    preprocess_dataset("USstates")


if __name__ == "__main__":
    main()
