Project
=======

This repository contains the code for the paper 
[Assaying Large-scale Testing Models to Interpret COVID-19 Case Numbers](https://arxiv.org/abs/2012.01912). 
It allows to reproduce all the results in the paper. In particular the scripts figure_2.py and figure_3.py were used to analyze the data and create the corresponding figures.

Data
====

The analysis is based on data from the following three sources:
- [The COVID Tracking Project](https://covidtracking.com/) collects data of the COVID-19 pandemic for all states of the US. The data in csv format can be 
  download [here](https://covidtracking.com/data/download) (might be removed in the future).
  This repository contains data downloaded on 2021-03-09.
- [Our World in Data](https://ourworldindata.org/coronavirus) collects data of the COVID-19 pandemic for most countries.
  The Coronavirus data can be downloaded
[here](https://github.com/owid/covid-19-data/tree/master/public/data). This repository contains data downloaded on 2021-03-09,
  testing data for Canada was added from the testing dataset in the same repository.
-  [DELVE Covid-19 Dataset](https://github.com/rs-delve/covid19_datasets) collects data related to
COVID-19. This repository only uses the covariates of countries. The
   dataset can be downloaded [here](https://github.com/rs-delve/covid19_datasets/tree/master/dataset).

Data references
----------
>  Max Roser, Hannah Ritchie, Esteban Ortiz-Ospina and Joe Hasell (2020) - "Coronavirus Pandemic (COVID-19)". Published online at OurWorldInData.org. Retrieved from: 'https://ourworldindata.org/coronavirus'

> Hasell, J., Mathieu, E., Beltekian, D. et al. A cross-country database of COVID-19 testing. Sci Data 7, 345 (2020). https://doi.org/10.1038/s41597-020-00688-8

> DELVE Global COVID-19 Dataset, https://github.com/rs-delve/covid19_datasets/blob/master/dataset/combined_dataset_latest.csv



 License
============

All code is completely open access 
under the 
[Creative Commons BY license](https://creativecommons.org/licenses/by/4.0/). 
You have the permission to use, distribute, and reproduce these in any medium, provided the source and authors are credited.
