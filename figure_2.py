""" Script to generate figure 2. Uses cross validation for death prediction to find the optimal parameters in the
testing model."""

import random
import torch
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import copy
from PIL import ImageColor
from sklearn.model_selection import KFold
import matplotlib.dates as mdates
from scipy.stats import wilcoxon

import models as md
from death_prediction import DeathInference

from preprocess import preprocess_dataset, set_selection_variable, get_selected_countries
import plotutils as pu
import covariates as covrts

rescaleFact = 10000
stest = wilcoxon
saveCoefs = True


def cross_validation(models, kfold=4, all_data=None, rmCountries=[], covariate_list=[], keepTorchObject=False,
                     gamma=.2):
    """Performs regression for the model parameters (potentially with covariates) using cross validation"""
    # exclude countries with less than 1000 deaths from the analysis
    min_death = 1000
    set_selection_variable(all_data, min_death, "death")
    countries_sel = get_selected_countries(all_data)
    countries_sel = [country for country in countries_sel if not (country.name in rmCountries)]
    random.shuffle(countries_sel)
    countries_selnames = [country.name for country in countries_sel]

    # covariate list contains list of covariates for the alpha regression.
    # As we do not have the data for the US states only the empty list is allowed.
    # US states are identified by 2 letter abb. while countries by their full name
    if len(countries_sel[0].name) == 2:
        covariate_list = []
    # gather covariates for the regression
    covariates, countries_with_metadata = get_meta_vars(countries_selnames, covariate_list)
    countries_sel = [country for country in countries_sel if country.name in countries_with_metadata]
    valLoss = []
    kf = KFold(n_splits=kfold)
    alphaTot = []

    # do cross validation
    for train_index, test_index in kf.split(countries_sel):
        countries_train = [countries_sel[kidx] for kidx in train_index]
        countries_val = [countries_sel[kidx] for kidx in test_index]
        alphaList = [prediction_regression(countries_train, mod, covariates=covariates[train_index, :], gamma=gamma) for
                     mod in models]
        alphaTot.append(alphaList)
        valLoss.append(list(zip(*[get_validation_loss_percountry_cov(countries_val, mod,
                                                                    alphaList[k], covariates=covariates[test_index, :],
                                                                    gamma=gamma) for k, mod in enumerate(models)])))

    def detach_torch(alpha):
        if isinstance(alpha, torch.Tensor):
            alpha = alpha.detach().numpy()
        return alpha

    if not keepTorchObject:
        alphaTot = [[detach_torch(alpha) for alpha in alphaList] for alphaList in alphaTot]

    alphaTot = list(zip(*alphaTot))
    return sum(valLoss, []), alphaTot, countries_sel, covariates


def get_validation_loss(data_objects, reverseEq, alpha, gamma=.2):
    """Returns average death prediction validation loss"""
    loss = torch.zeros(1, dtype=torch.double)
    di = DeathInference(gamma)
    for c in data_objects:
        loss += di(torch.tensor(c.conf, dtype=torch.double),
                   torch.tensor(c.test / c.population, dtype=torch.double),
                   torch.tensor(c.death, dtype=torch.double), alpha,
                   reverseEq)
    return loss / len(data_objects)


def get_validation_loss_percountry(data_objects, reverseEq, alpha, gamma=.2):
    """Returns death prediction validation loss as a list"""
    di = DeathInference(gamma)
    loss = []
    for c in data_objects:
        loss.append(di(torch.tensor(c.conf, dtype=torch.double),
                       torch.tensor(c.test / c.population, dtype=torch.double),
                       torch.tensor(c.death, dtype=torch.double), alpha,
                       reverseEq).detach().numpy())
    return loss


def get_validation_loss_percountry_cov(data_objects, model, alpha, covariates=[], gamma=.2):
    """Returns death prediction validation loss for models with covariates as a list"""
    di = DeathInference(gamma)
    loss = []
    if covariates == []:
        covariates = [[]] * len(data_objects)
    for kcount, c in enumerate(data_objects):
        covVec = np.concatenate([[1], covariates[kcount, :]])[np.newaxis, :]  # ,dtype=torch.double)

        if alpha is None:
            loss.append(di(torch.tensor(c.conf / rescaleFact, dtype=torch.double),
                           torch.tensor(c.test / c.population, dtype=torch.double),
                           torch.tensor(c.death, dtype=torch.double), alpha,
                           model.reverseEq).detach().numpy())
        else:
            loss.append(di(torch.tensor(c.conf / rescaleFact, dtype=torch.double),
                           torch.tensor(c.test / c.population, dtype=torch.double),
                           torch.tensor(c.death, dtype=torch.double),
                           (torch.tensor(covVec, dtype=torch.double).matmul(alpha)),
                           model.reverseEq).detach().numpy())
    return loss


def prediction_regression(data_objects, model, covariates=None, gamma=.2):
    """Optimizes model parameters by minimizing prediction loss"""
    if model.alpha is None:
        return None
    alpha = torch.ones((1 + covariates.shape[1], model.alpha.shape[0]), requires_grad=True, dtype=torch.double)

    optimizer = torch.optim.LBFGS([alpha], max_iter=200, lr=.1, tolerance_grad=1e-5, )
    nr_countries = len(data_objects)
    y_list = []
    t_list = []
    d_list = []
    cov_matrix = torch.ones((nr_countries, 1), dtype=torch.double)
    if covariates is not None:
        cov_matrix = torch.cat((cov_matrix, torch.tensor(covariates, dtype=torch.double)), dim=1)
    for kcount, country in enumerate(data_objects):
        y_list.append(torch.tensor(country.conf / rescaleFact, dtype=torch.double))
        t_list.append(torch.tensor(country.test / country.population, dtype=torch.double))
        d_list.append(torch.tensor(country.death, dtype=torch.double))
    di = DeathInference(gamma)

    def closure():
        optimizer.zero_grad()
        alpha_acc = torch.matmul(cov_matrix, alpha)
        loss = torch.zeros(1, dtype=torch.double)
        for i in range(nr_countries):
            loss += di(y_list[i], t_list[i], d_list[i], alpha_acc[i:(i + 1), :], model.reverseEq)
        if getattr(loss, "grad_fn", None) is None:
            return None
        loss.backward(retain_graph=False)
        return loss

    loss = closure()
    if loss is None:
        print('no optimization to perform')
        return None
    for kite in range(3):
        optimizer.step(closure)
    return alpha


def get_meta_vars(countries_list, metaVarList=[]):
    """Collects country covariates for the regression"""
    if metaVarList == []:
        return np.zeros((len(countries_list), 1)), countries_list
    country_dict = covrts.get_COVID_DELVE_data(countries_list)
    if metaVarList is None:
        metaVarList = ['stats_population_density', 'stats_median_age', 'stats_gdp_per_capita',
                       'stats_smoking', 'stats_population_urban',
                       'stats_population_school_age']
    metaVars = [[country_dict[country]["Metadata"][var] for var in metaVarList] for country in country_dict.keys()]
    metaVars = np.array(metaVars)
    population = [country_dict[country]["Metadata"]['stats_population'] for country in country_dict.keys()]
    # Normalize urban population and population in school age
    if 'stats_population_urban' in metaVarList:
        index = metaVarList.index('stats_population_urban')
        metaVars[:, index] = metaVars[:, index] / population
    if 'stats_population_school_age' in metaVarList:
        index = metaVarList.index('stats_population_school_age')
        metaVars[:, index] = metaVars[:, index] / population
    country_list = np.array([country for country in country_dict.keys()])
    print("The following countries were dropped due to missing Metadata:")
    print(country_list[np.isnan(metaVars).any(axis=1)])

    country_list = country_list[~np.isnan(metaVars).any(axis=1)]
    # Normalisation of covariates
    metaVars = metaVars[~np.isnan(metaVars).any(axis=1)]
    metaVars = metaVars - np.mean(metaVars, axis=0, keepdims=True)
    metaVars = metaVars / np.sqrt(np.var(metaVars, axis=0, keepdims=True))
    return metaVars, country_list


def get_death_pred(Y, T, alpha, reverseEq, gamma=.2):
    """
    Computes prediction of death rate based on confirmed cases, test rate and test model.

    Function is called by: getDeathPredAll, getQuantiles, getLikelihoods
    """
    # Death filter is taken from Flaxman:
    death_filter = np.array([2.3973e-08, 4.3723e-06, 5.0811e-05, 2.5835e-04, 8.2425e-04, 1.9629e-03,
                             3.8389e-03, 6.5244e-03, 9.9860e-03, 1.4096e-02, 1.8658e-02, 2.3437e-02,
                             2.8191e-02, 3.2693e-02, 3.6750e-02, 4.0215e-02, 4.2986e-02, 4.5010e-02,
                             4.6275e-02, 4.6806e-02, 4.6655e-02, 4.5893e-02, 4.4605e-02, 4.2882e-02,
                             4.0815e-02, 3.8493e-02, 3.5996e-02, 3.3399e-02, 3.0765e-02, 2.8148e-02,
                             2.5593e-02, 2.3132e-02, 2.0794e-02, 1.8595e-02, 1.6548e-02, 1.4660e-02,
                             1.2931e-02, 1.1359e-02, 9.9398e-03, 8.6661e-03, 7.5294e-03, 6.5202e-03,
                             5.6286e-03, 4.8443e-03, 4.1574e-03, 3.5581e-03, 3.0373e-03, 2.5863e-03,
                             2.1969e-03, 1.8619e-03, 1.5556e-03, 1.2599e-03, 1.0099e-03, 8.0441e-04,
                             6.3791e-04, 5.0417e-04, 3.9740e-04, 3.1254e-04, 2.4534e-04])
    I_est = reverseEq(y=Y, T=T, alpha=alpha)
    I_new = np.zeros_like(I_est)
    I_new[1:] = I_est[1:] - (1 - gamma) * I_est[:-1]
    D_pred = np.convolve(I_new, death_filter, 'full')[:I_new.size]
    return D_pred


def predict_death_numpy(c, pModel, alpha, gamma=.2):
    """Smoothed death prediction for plotting"""
    smoothing_filter = np.array([1., 1., 1., 1., 1., 1., 1.]) / 7
    eps = 1e-9
    D = c.death
    D = np.convolve(D, smoothing_filter, 'same')
    D = D[:-len(smoothing_filter)]
    if pModel is None:
        return D
    D_pred = get_death_pred(c.conf, c.test / c.population, alpha, pModel.reverseEq, gamma)
    D_pred = np.convolve(D_pred, smoothing_filter, 'same')
    D_pred = D_pred[:-len(smoothing_filter)]
    D_pred = D_pred * np.sum(D) / np.sum(D_pred)
    D_pred = np.maximum(D_pred, eps)
    return D_pred

random.seed(1)
plt.rcParams.update({'font.size': 12})

pModel = md.PoissonModel()
bModel = md.BaselineModel()
paModel = md.PoisAlphaModel(torch.ones((1,), dtype=torch.double))
powModel = md.PowerModel(torch.ones((1,), dtype=torch.double))
constModel = md.ConstantModel()
quotModel = md.QuotientModel(torch.ones((2,), dtype=torch.double))
prelModel = md.PoisRelAlphaModel(torch.ones((1,), dtype=torch.double))
psModel = md.PoisSatModel(torch.ones((2,), dtype=torch.double))
addModel = md.AdditiveModel(torch.ones((1,), dtype=torch.double))

data_World = preprocess_dataset("World", local=True)
data_US = preprocess_dataset("USstates", local=True)

# gamma denotes the recovery parameter in the SIR model
gamma = .2

models = [bModel, pModel, paModel, addModel, ]

plt.tight_layout()
fig, ax = plt.subplots(2, 3, figsize=[18, 12])
panelLetter = [['B', 'C', 'D'], ['E', 'F', 'G']]


#######################################################################
# Do regression to determine model parameters (without covariates here)

modLabels = [mod.name for mod in models]
rmCountries = []
covariate_list = []
data_World_copy = copy.deepcopy(data_World)
valLoss, alphas, countries_list, covariates = cross_validation(models, all_data=data_World_copy, kfold=10,
                                                               rmCountries=rmCountries, covariate_list=covariate_list,
                                                               gamma=gamma)

print("There are ", len(countries_list), "countries used for the analysis")

# vals collects the list of validation loss per country (2nd index) and model (1st index)
vals = np.array(list(zip(*valLoss)))
mask = ~np.isnan(vals)
filtered_data = [d[m] for d, m in zip(vals, mask)]

# performing tests for the significance of differen models
testchange = stest(vals[1, :] - vals[0, :]).pvalue
testupsat = stest(vals[1, :] - vals[2, :]).pvalue
testdownsat = stest(vals[1, :] - vals[3, :]).pvalue
testupdown = stest(vals[2, :] - vals[3, :]).pvalue

shortModLabs = []
for k, lname in enumerate(modLabels):
    if lname == 'Up-saturating':
        shortModLabs.append('Up-sat.')
    elif lname == 'Down-saturating':
        shortModLabs.append('Down-sat.')
    else:
        shortModLabs.append(lname)

if saveCoefs:
    np.save('./parameters/alphasOptim.npy', alphas)
    np.save('./parameters/modelsOptim.npy', models)

##########################################################
# Create plot illustrating behaviour of the testing models

T = np.logspace(0.5, 4.5)

plotTrueAlpha = True

alpha_plot = np.exp(np.array([10, 5]))

if plotTrueAlpha:
    alphas = np.load('./parameters/alphasOptim.npy', allow_pickle=True)
    alphaMed = [np.nanmedian(np.stack(alph)[:, :, 0], axis=0) if not (alph[0] is None) else None for alph in alphas]
    alpha_plot = (np.array([alphaMed[2][0], alphaMed[3][0]]))

    ax[0, 0].loglog(T, bModel.forwardEq(None, T, None) * 30, linewidth=2)
    ax[0, 0].loglog(T, pModel.forwardEq(None, T, None), linewidth=2)
    ax[0, 0].loglog(T, paModel.forwardEq(None, T, alpha_plot[0:1]) * alpha_plot[0:1] / 1.2, linewidth=2)
    ax[0, 0].loglog(T, addModel.forwardEq(None, T, alpha_plot[1:2]) * 1.2, linewidth=2)

    ax[0, 0].set_ylabel('Rate of confirmed cases/number of infectious')
    ax[0, 0].set_xlabel('Tests/day/$10^6$ inhabitants')

    ax[0, 0].loglog([alpha_plot[1], alpha_plot[1]], [1, addModel.forwardEq(None, alpha_plot[1], alpha_plot[1:2]) * 1.2],
                    'k--')
    ax[0, 0].loglog([alpha_plot[0], alpha_plot[0]],
                    [1, paModel.forwardEq(None, alpha_plot[0], alpha_plot[0:1]) * alpha_plot[0] / 1.2], 'k--')
    ax[0, 0].set_ylim([2, 10000])
    ax[0, 0].annotate("", xy=(alpha_plot[0], 10), xytext=(alpha_plot[1], 10),
                      arrowprops=dict(arrowstyle="<->"))
    ax[0, 0].text(250, 11, 'Linearity range', horizontalalignment='center')

else:
    ax[0, 0].loglog(T, bModel.forwardEq(None, T, None) * 100, linewidth=2)
    ax[0, 0].loglog(T, pModel.forwardEq(None, T, None), linewidth=2)
    ax[0, 0].loglog(T, paModel.forwardEq(None, T, alpha_plot[0:1]) / 2 * alpha_plot[0:1], linewidth=2)
    ax[0, 0].loglog(T, addModel.forwardEq(None, T, alpha_plot[1:2]) * 2, linewidth=2)

    ax[0, 0].set_ylabel('Y/I')
    ax[0, 0].set_xlabel('T')

ax[0, 0].legend([mod.name for mod in models])
ax[0, 0].spines['right'].set_visible(False)
ax[0, 0].spines['top'].set_visible(False)
ax[0, 0].text(-0.02, 1.08, panelLetter[0][0], transform=ax[0][0].transAxes,
              fontsize=16, fontweight='bold', va='top', ha='right')
ax[0, 0].set_title('Testing models')

############################################################
# Create boxplots to compare performance of different models

bplot = pu.simpleBoxMatchedSamp(ax[0, 1], filtered_data, labels=shortModLabs, y=1.8, patch_artist=True)

ax[0, 1].text(3.2, 1.2, '*  p<.05\n** p<.005', bbox=dict(facecolor='none', edgecolor='black'))

height = [1.2, .8, .7, .9]
center = [1., 2., 3., 4.]
pu.barplot_annotate_brackets(ax[0, 1], 0, 1, testchange, center, height, maxasterix=2)

pu.barplot_annotate_brackets(ax[0, 1], 1, 2, testupsat, center, height, maxasterix=2)

pu.barplot_annotate_brackets(ax[0, 1], 1, 3, testdownsat, center, height, maxasterix=2)

prop_cycle = plt.rcParams['axes.prop_cycle']
colors = prop_cycle.by_key()['color']
for patch, color in zip(bplot['boxes'], colors):
    patch.set_facecolor(color)
ax[0, 1].spines['right'].set_visible(False)
ax[0, 1].spines['top'].set_visible(False)
ax[0, 1].text(-0.02, 1.08, panelLetter[0][1], transform=ax[0][1].transAxes,
              fontsize=16, fontweight='bold', va='top', ha='right')
ax[0, 1].set_title('Prediction error (World countries)')


##################################################################################
# Create raster plot to indicate difference between baseline and best testing model

rmCountries = []
curax = ax[0, 2]
showRaster = True
# show raster for comparison of models or show us states results
if showRaster:
    curax.plot(vals.T[:, 0], vals.T[:, 2], '.')
    curax.set_xlabel(modLabels[0] + ' testing error')
    curax.set_ylabel(modLabels[2] + ' testing error')
    curax.plot([0, 1], [0, 1], 'k--')
    curax.axis('equal')
    curax.set_ylim([0., 1])
    curax.set_xlim([0., 1.2])
else:
    covariate_list = []
    data_US_copy = copy.deepcopy(data_US)
    state_valLoss, state_alphas, state_list, _ = cross_validation(models, all_data=data_US_copy, kfold=10,
                                                                  rmCountries=rmCountries,
                                                                  covariate_list=covariate_list)
    state_vals = np.array(list(zip(*state_valLoss)))

    # remove nans for boxplot
    mask = ~np.isnan(state_vals)
    state_filtered_data = [d[m] for d, m in zip(state_vals, mask)]
    testchange = stest(state_vals[1, :] - state_vals[0, :]).pvalue

    pu.simpleBoxMatchedSamp(curax, state_filtered_data, labels=modLabels, testchange=testchange)
    curax.spines['right'].set_visible(False)
    curax.spines['top'].set_visible(False)
    curax.set_title('US states')
    curax.set_ylabel('Prediction error')
    curax.set_xticklabels(modLabels)

curax.text(-0.02, 1.08, panelLetter[0][2], transform=curax.transAxes,
           fontsize=16, fontweight='bold', va='top', ha='right')
curax.spines['right'].set_visible(False)
curax.spines['top'].set_visible(False)

#
#

alphaMed = [np.nanmedian(np.stack(alph)[:, :, 0], axis=0) if not (alph[0] is None) else None for alph in alphas]
D_pred = list(zip(
    *[[predict_death_numpy(c, mod, alphaMed[kmod], gamma) for k, c in enumerate(countries_list)] for kmod, mod in
      enumerate(models[:3] + [None])]))

D_pred = [np.array(D).T for D in D_pred]

modName = [mod.name if mod is not None else 'Observed death rate' for mod in models[:3] + [None]]

countPlot = ['United States', 'United Kingdom', 'France']  # 'Bangladesh'
countIdx = [k for k, count in enumerate(countries_list) if count.name in countPlot]
for kc in range(len(countIdx)):
    # We use smoothing over one week.
    ax[1, kc].plot(countries_list[countIdx[kc]].dates[:-7], D_pred[countIdx[kc]][:, :-1], linewidth=2)
    # plot the last (actual death rate) in black
    ax[1, kc].plot(countries_list[countIdx[kc]].dates[:-7], D_pred[countIdx[kc]][:, -1], 'k', linewidth=2)
    ax[1, kc].set_title(countries_list[countIdx[kc]].name)
    ax[1, kc].legend(modName)
    ax[1, kc].spines['right'].set_visible(False)
    ax[1, kc].spines['top'].set_visible(False)
    ax[1, kc].xaxis.set_major_locator(mdates.MonthLocator())
    ax[1, kc].xaxis.set_major_formatter(mdates.DateFormatter('%b'))

panelLetterRow = panelLetter[1]
for kc in range(len(countIdx)):
    ax[1, kc].text(-0.02, 1.08, panelLetterRow[kc], transform=ax[1, kc].transAxes,
                   fontsize=16, fontweight='bold', va='top', ha='right')

ax[1, 0].set_ylabel('Deaths per day')

fig.savefig("./figures/Fig2.pdf", bbox_inches='tight')


# print CV alpha values for both models
# up-saturating
alphaCVValsUp = np.concatenate(alphas[2],axis=1)[0]
alphaCVValsUp
# down-saturating
alphaCVValsDown = np.concatenate(alphas[3],axis=1)[0]
alphaCVValsDown

############################ same for US #################################################

sfig, sax = plt.subplots(1, 3, figsize=[18, 6])

sax.shape = (1, 3)

rmCountries = []
covariate_list = ['stats_gdp_per_capita']
data_US_copy = copy.deepcopy(data_US)
valLossUS, alphasUS, countries_listUS, covariatesUS = cross_validation(models, all_data=data_US_copy, kfold=10,
                                                                       rmCountries=rmCountries,
                                                                       covariate_list=covariate_list, gamma=gamma)
valsUS = np.array(list(zip(*valLossUS)))
print("There are ", len(countries_listUS), "states in the US used for the analysis")
if saveCoefs:
    np.save('./parameters/alphasOptimUS.npy', alphasUS)
    np.save('./parameters/modelsOptimUS.npy', models)
# remove nans for boxplot
mask = ~np.isnan(valsUS)
filtered_data = [d[m] for d, m in zip(valsUS, mask)]

test_adap = stest(valsUS[1, :] - valsUS[0, :]).pvalue
test_lim_up = stest(valsUS[1, :] - valsUS[2, :]).pvalue
test_lim_down = stest(valsUS[1, :] - valsUS[3, :]).pvalue
test_lim_up_down = stest(valsUS[2, :] - valsUS[3, :]).pvalue

T = np.logspace(.5, 4.5)

if plotTrueAlpha:
    alphas = np.load('./parameters/alphasOptimUS.npy', allow_pickle=True)
    alphaMed = [np.nanmedian(np.stack(alph)[:, :, 0], axis=0) if not (alph[0] is None) else None for alph in alphas]
    alpha_plot = (np.array([alphaMed[2][0], alphaMed[3][0]]))

    sax[0, 0].loglog(T, bModel.forwardEq(None, T, None) * 30, linewidth=2)

    sax[0, 0].loglog(T, pModel.forwardEq(None, T, None), linewidth=2)
    sax[0, 0].loglog(T, paModel.forwardEq(None, T, alpha_plot[0:1]) * alpha_plot[0:1] / 1.2, linewidth=2)
    sax[0, 0].loglog(T, addModel.forwardEq(None, T, alpha_plot[1:2]) * 1.2, linewidth=2)
    sax[0, 0].set_ylabel('Rate of confirmed cases/number of infectious')

    sax[0, 0].set_xlabel('Tests/day/$10^6$ inhabitants')

    sax[0, 0].loglog([alpha_plot[1], alpha_plot[1]],
                     [1, addModel.forwardEq(None, alpha_plot[1], alpha_plot[1:2]) * 1.2], 'k--')
    sax[0, 0].loglog([alpha_plot[0], alpha_plot[0]],
                     [1, paModel.forwardEq(None, alpha_plot[0], alpha_plot[0:1]) * alpha_plot[0] / 1.2], 'k--')
    sax[0, 0].set_ylim([2, 10000])
    sax[0, 0].annotate("", xy=(alpha_plot[0], 10), xytext=(alpha_plot[1], 10),
                       arrowprops=dict(arrowstyle="<->"))
    sax[0, 0].text(850, 11, 'Linearity range', horizontalalignment='center')

else:
    alpha_plot = np.exp(np.array([10, 5]))
    sax[0, 0].loglog(T, bModel.forwardEq(None, T, None) * 100, linewidth=2)

    sax[0, 0].loglog(T, pModel.forwardEq(None, T, None), linewidth=2)
    sax[0, 0].loglog(T, paModel.forwardEq(None, T, alpha_plot[0:1]) / 2 * alpha_plot[0:1], linewidth=2)
    sax[0, 0].loglog(T, addModel.forwardEq(None, T, alpha_plot[1:2]) * 2, linewidth=2)

    sax[0, 0].set_ylabel('Y/I')

    sax[0, 0].set_xlabel('T')

sax[0, 0].legend([mod.name for mod in models])
sax[0, 0].spines['right'].set_visible(False)
sax[0, 0].spines['top'].set_visible(False)
sax[0, 0].text(-0.02, 1.08, 'A', transform=sax[0][0].transAxes,
               fontsize=16, fontweight='bold', va='top', ha='right')
sax[0, 0].set_title('Testing models')


bplot = pu.simpleBoxMatchedSamp(sax[0, 1], filtered_data, labels=shortModLabs, y=1.8, patch_artist=True)

height = [1., .45, .36, .35]
height2 = [1., .73, .4, .73]
center = [1., 2., 3., 4.]
pu.barplot_annotate_brackets(sax[0, 1], 0, 1, test_adap, center, height, maxasterix=2)
pu.barplot_annotate_brackets(sax[0, 1], 1, 2, test_lim_up, center, height, maxasterix=2)
pu.barplot_annotate_brackets(sax[0, 1], 1, 3, test_lim_down, center, height2, maxasterix=2)
pu.barplot_annotate_brackets(sax[0, 1], 2, 3, test_lim_up_down, center, height, maxasterix=2)
sax[0, 1].text(2.5, 1., '** p<.005', bbox=dict(facecolor='none', edgecolor='black'))

prop_cycle = plt.rcParams['axes.prop_cycle']
colors = prop_cycle.by_key()['color']
for patch, color in zip(bplot['boxes'], colors):
    patch.set_facecolor(color)
sax[0, 1].spines['right'].set_visible(False)
sax[0, 1].spines['top'].set_visible(False)
sax[0, 1].text(-0.02, 1.08, 'B', transform=sax[0][1].transAxes,
               fontsize=16, fontweight='bold', va='top', ha='right')
sax[0, 1].set_title('Prediction error (US states)')
rmCountries = []
cursax = sax[0, 2]
showRaster = True
# show raster for comparison of models or show us states results
if showRaster:
    cursax.plot(valsUS.T[:, 0], valsUS.T[:, 2], '.')
    cursax.set_xlabel(modLabels[0] + ' testing error')
    cursax.set_ylabel(modLabels[2] + ' testing error')
    cursax.plot([0, 1], [0, 1], 'k--')
    cursax.axis('equal')

    cursax.set_ylim(bottom=0., top=1.)
    cursax.set_xlim(left=0., right=1.)
    # cursax.plot([0,1],[0,1],'--')

cursax.text(-0.02, 1.08, 'C', transform=cursax.transAxes,
            fontsize=16, fontweight='bold', va='top', ha='right')
cursax.spines['right'].set_visible(False)
cursax.spines['top'].set_visible(False)

sfig.savefig("./figures/sFig2.pdf", bbox_inches='tight')


########### Plot additional figures, same boxplot diagram
# Compare testing models with and without covariates and plot boxplots

random.seed(1)
# Direct comparison of covariates

modelsCov = [pModel, paModel, addModel]
modCovLabels = [mod.name for mod in modelsCov]
rmCountries = ['Belarus', 'Croatia', 'Macedonia']
covariate_list = ['stats_population_density', 'stats_population_urban']
covariates = ['stats_population_density', 'stats_median_age', 'stats_gdp_per_capita',
              'stats_smoking', 'stats_population_urban',
              'stats_population_school_age']
# gamma = 1

# compare no covariates with covariate regresssion
list_of_covariate_list = [[], ['stats_gdp_per_capita', 'stats_population_density', 'stats_population_urban']]

label_covariates = 'with covariates'
loss_list = []
alpha_list = []
for m in modelsCov:
    for cov in list_of_covariate_list:
        data_World_copy = copy.deepcopy(data_World)
        valLoss, alphastmp, countries_list, covariates = cross_validation([m], all_data=data_World_copy, kfold=10,
                                                                          rmCountries=rmCountries, covariate_list=cov,
                                                                          gamma=gamma)
        valsCov = np.array(list(zip(*valLoss)))
        valsCov = np.squeeze(valsCov, axis=0)
        country_names = [c.name for c in countries_list]
        df_vals = pd.DataFrame({'value': valsCov, 'country': country_names})
        df_vals['Testing model'] = m.name
        if cov == []:
            df_vals['Covariates'] = 'no covariates'
        else:
            df_vals['Covariates'] = label_covariates
        loss_list.append(df_vals)
        alpha_list.append(alphastmp)

replaceBaselinePlot = False
if replaceBaselinePlot:
    loss_list[1] = loss_list[0]
    loss_list[1]['Covariates'] = label_covariates

df = pd.concat(loss_list, ignore_index=True)
df = df.sort_values('country', ignore_index=True)
df.to_csv('./parameters/buffer_df.csv')

df = pd.read_csv('./parameters/buffer_df.csv')
label_covariates = 'with covariates'
figCov, axCov = plt.subplots(1, 1, figsize=[12, 6])
prop_cycle = plt.rcParams['axes.prop_cycle']
colors = prop_cycle.by_key()['color']
pal = {}
for i in range(3):
    r, g, b = ImageColor.getrgb(colors[i + 1])
    dictRef = {'no covariates': (r, g, b, 1), label_covariates: (r, g, b, .5)}
    if i == 0:
        pal['Limiting'] = dictRef
    if i == 1:
        pal['Up-saturating'] = dictRef
    if i == 2:
        pal['Down-saturating'] = dictRef

cov_change = []
pd.set_option("display.max_rows", None, "display.max_columns", None)
modelsCov = [pModel, paModel, addModel]
modelsCov.pop(0)

# extract only countries where covariates are available
data_without = df[df['Covariates'] == 'no covariates']
data_cov = df[df['Covariates'] == label_covariates]
countries_1 = set(data_without['country'].unique())
countries_2 = set(data_cov['country'].unique())
country_intersection = countries_1.intersection(countries_2)
df = df[df['country'].isin(country_intersection)]
for m in modelsCov:
    data_1 = df[(df['Testing model'] == m.name) & (df['Covariates'] == 'no covariates')]
    data_2 = df[(df['Testing model'] == m.name) & (df['Covariates'] == label_covariates)]
    cov_change.append(wilcoxon(data_1['value'], data_2['value']).pvalue)

adp_change = []
for m in modelsCov:
    data_1 = df[(df['Testing model'] == m.name) & (df['Covariates'] == 'no covariates')]
    data_2 = df[(df['Testing model'] == pModel.name) & (df['Covariates'] == 'no covariates')]
    adp_change.append(wilcoxon(data_1['value'], data_2['value']).pvalue)

adp_cov_change = []
for m in modelsCov:
    data_1 = df[(df['Testing model'] == m.name) & (df['Covariates'] == label_covariates)]
    data_2 = df[(df['Testing model'] == pModel.name) & (df['Covariates'] == 'no covariates')]
    adp_cov_change.append(wilcoxon(data_1['value'], data_2['value']).pvalue)

one_column_limiting = True
if one_column_limiting:
    df = df[(df['Covariates'] != label_covariates) | (df['Testing model'] != pModel.name)]

sns.boxplot(x='Testing model', y='value', data=df, hue='Covariates', ax=axCov,
            order=['Limiting', 'Up-saturating', 'Down-saturating'])

height = 6 * [.5]
center = [-.2, .2, .8, 1.2, 1.8, 2.2]
for i in range(2):
    pu.barplot_annotate_brackets(axCov, 2 + 2 * i, 3 + 2 * i, cov_change[i], center, height, maxasterix=2)

axCov.spines['right'].set_visible(False)
axCov.spines['top'].set_visible(False)
axCov.set_title('Prediction error (world countries)')
axCov.set_xlabel('')
axCov.set_ylabel('')

figCov.savefig("./figures/sFig2_covariates.pdf")

# print elementary statistical variables
# number of individuals for each analysis
vals.shape
valsUS.shape
valsCov.shape

