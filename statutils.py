# -*- coding: utf-8 -*-
"""
Created on Tue Nov 17 17:49:41 2020

@author: besserve
"""
""" Create relevant p-values based on regression slope frames."""

import pandas as pd
import numpy as np
from scipy.stats import wilcoxon


def defineFrames (confFit,fullFit,testFit,dataName):
    nsel = len(confFit['pre']['slope'])
    effect = pd.DataFrame({'Variable (log)':[dataName[0]]*nsel+[dataName[1]]*nsel,
                      'Effect':-np.concatenate([np.squeeze(confFit['post']['slope'])-np.squeeze(confFit['pre']['slope']),
                                                     np.squeeze(fullFit['post']['slope'])-np.squeeze(fullFit['pre']['slope'])])})
    melt_effect=pd.melt(effect,id_vars=['Variable (log)'],value_vars=['Effect'],var_name='Effect')
    df = pd.DataFrame({'Variable (log)':[dataName[0]]*nsel+[dataName[2]]*nsel+[dataName[1]]*nsel,
                      'Pre-lockdown':np.concatenate([np.squeeze(confFit['pre']['slope']),
                                                     np.squeeze(testFit['pre']['slope']),np.squeeze(fullFit['pre']['slope'])]),
                       'During lockdown':np.concatenate([np.squeeze(confFit['post']['slope']),np.squeeze(testFit['post']['slope']),np.squeeze(fullFit['post']['slope'])]).T})
    dd=pd.melt(df,id_vars=['Variable (log)'],value_vars=['Pre-lockdown','During lockdown'],var_name='Period')
    
    
    effectorig = +np.squeeze(confFit['pre']['slope'])-np.squeeze(confFit['post']['slope'])
    effectcorrec = -np.squeeze(fullFit['post']['slope'])+np.squeeze(fullFit['pre']['slope'])
    
    effectdiff = -np.squeeze(fullFit['post']['slope'])+np.squeeze(fullFit['pre']['slope'])-np.squeeze(confFit['pre']['slope'])+np.squeeze(confFit['post']['slope'])
    r0orig = np.squeeze(confFit['post']['slope'])
    r0corr = np.squeeze(fullFit['post']['slope'])

    r0origpre = np.squeeze(confFit['pre']['slope'])
    r0corrpre = np.squeeze(fullFit['pre']['slope'])
    
    tslopepre = np.squeeze(testFit['pre']['slope'])
    tslope = np.squeeze(testFit['post']['slope'])


    rorigmedianpre = np.median(r0origpre)
    rcorrecmedianpre = np.median(r0corrpre)
    
    stest = wilcoxon
    
    # statistics for correcting slopes, pre and post lockdown
    pr0correcpre = stest(r0origpre-r0corrpre).pvalue
    pr0correcpost = stest(r0orig-r0corr).pvalue
    
    # statistics of non-vanishing slopes post lock down (pre obvious and not too relevant)
    porig = stest(r0orig).pvalue
    pcorrec = stest(r0corr).pvalue
    
    porigpre = stest(r0origpre).pvalue
    pcorrecpre = stest(r0corrpre).pvalue
    
    
    # statistics of causal effect without and with correction
    peffectorig = stest(effectorig).pvalue
    peffectcorrec = stest(effectcorrec).pvalue
    
    # statitics of correcting causal effect
    peffect = stest(effectdiff).pvalue
    
    # statistics of change in test slope
    testchange = stest(tslopepre-tslope).pvalue

    pvals = {'pr0correcpre':pr0correcpre,
             'pr0correcpost':pr0correcpost,
             'rorigmedianpre':rorigmedianpre,
             'rcorrecmedianpre':rcorrecmedianpre,
             'porig':porig,
             'pcorrec':pcorrec,
             'porigpre':porigpre,
             'pcorrecpre':pcorrecpre,
             'peffectorig':peffectorig,
             'peffectcorrec':peffectcorrec,
             'peffect':peffect,
             'testchange':testchange}
    
    return melt_effect, dd, pvals
