# -*- coding: utf-8 -*-
"""
This library gathers the functions we need for glm-like estimation

Created on Sat Sep  5 08:18:13 2020

@author: besserve
"""
import numpy as np
import statsmodels.api as sm

# copy familly definitions from sm
Poisson_family = sm.families.Poisson
Binomial_family = sm.families.Binomial

# define dictionaries to make the class to sm.glm indepedent from the family

def poissonPred(y, T, alpha):
    return y


def binomialPred(y, T, alpha):
    return y / T


def invPoissonPred(y, T, alpha):
    return y


def invBinomialPred(y, T, alpha):
    return y * T


famMap = {'poisson': Poisson_family, 'binomial': Binomial_family}
predMap = {'poisson': poissonPred, 'binomial': binomialPred}
invPredMap = {'poisson': invPoissonPred, 'binomial': invBinomialPred}


def addIntercept(x):
    if x.ndim == 1:
        X = np.concatenate([x[:, np.newaxis], np.ones([len(x), 1])], axis=1)
    else:
        X = np.concatenate([x, np.ones([x.shape[0], 1])], axis=1)
    return X


def addVariable(x, v):
    if v.ndim == 1:
        v = v[:, np.newaxis]
    if x.ndim == 1:
        X = np.concatenate([x[:, np.newaxis], v], axis=1)
    else:
        X = np.concatenate([x, v], axis=1)
    return X


def policyVec(t, policyDates, excludeIntervals=[]):
    '''
    
    Generate indicator functions for each interval delimited by policyDates

    Parameters
    ----------
    t : TYPE
        DESCRIPTION.
    policyDates : TYPE
        DESCRIPTION.
    excludeIntervals : TYPE, optional
        DESCRIPTION. The default is [].

    Returns
    -------
    pVec : numpy array (time x intervals)
        indicator array.
    exclIdxVec : numpy logical array (time x)
        DESCRIPTION.

    '''
    if any(policyDates < np.min(t)) or any(policyDates > np.max(t)):
        raise ValueError('policy dates out of time range')

    idxVec = [[]] * (len(policyDates) + 1)
    # Just a vector of false?
    exclIdxVec = (t == 0) & (t != 0)
    prevDate = None
    for kd, date in enumerate(policyDates):
        idxVec[kd] = t < date
        if kd == 0:
            idxVec[kd] = t < date
        else:
            idxVec[kd] = (t < date) * (t >= prevDate)
        prevDate = date
    if len(policyDates) == 0:
        idxVec[len(policyDates)] = 0 * t + 1
    else:
        idxVec[len(policyDates)] = t >= policyDates[-1]
        excludeIntervals.sort()
        for ke, exclIdx in enumerate(excludeIntervals):
            exclIdxVec = exclIdxVec | idxVec[exclIdx - ke]
            del idxVec[exclIdx - ke]

    # add zero to convert logical to integer
    pVec = np.stack(idxVec, axis=-1) + 0
    return pVec, exclIdxVec


def policy_withinMats(t, policyDates, excludeIntervals=[]):
    """
    Generates two matrices to compute the within mean of each interval
    Parameters
    ----------
    t : TYPE
        DESCRIPTION.
    policyDates : TYPE
        DESCRIPTION.
    excludeIntervals : TYPE, optional
        DESCRIPTION. The default is [].

    Returns
    -------
    M1 : 2D numpy array (interval x time)
        matmul(M1,X) will return a vector of the interval averages of X (number of intervals x 1).
    M2 : 2D numpy array (time x interval)
        matmul(M1,mu) will return a vector with the interval average put at each time point.

    """
    pVars, exclIdxVec = policyVec(t, policyDates, excludeIntervals=excludeIntervals)
    M1 = pVars / np.sum(pVars, axis=0, keepdims=True)
    M1 = M1.T
    M2 = np.copy(pVars)
    np.delete(M1, excludeIntervals, axis=1)
    return M1, M2


def policyVars(t, policyDates, funcType='linear', excludeIntervals=[]):
    pVars, exclIdxVec = policyVec(t, policyDates, excludeIntervals=excludeIntervals)
    excludeIntervals.sort()
    selInter = [x for x in range(len(policyDates) + 1) if x not in excludeIntervals]
    pLabels = ['pintercept' + str(k) for k in selInter]
    if funcType == 'linear':
        pVars = addVariable(pVars, pVars * t[:, np.newaxis])
        pLabels = pLabels \
                  + ['pslope' + str(k) for k in selInter]
    return pVars, pLabels, exclIdxVec


def glmIndivFit(y, t, T, policyDates, glmFamily, forwardEq, excludeIntervals=[], alpha=None):
    """
    Individual country glm fit based on poisson regression

    Parameters
    ----------
    y : TYPE
        DESCRIPTION.
    t : TYPE
        DESCRIPTION.
    T : TYPE
        DESCRIPTION.
    policyDates : TYPE
        DESCRIPTION.
    glmFamily : TYPE
        DESCRIPTION.
    forwardEq : TYPE
        DESCRIPTION.
    excludeIntervals : TYPE, optional
        DESCRIPTION. The default is [].
    alpha : TYPE, optional
        DESCRIPTION. The default is None.

    Returns
    -------
    ypred : TYPE
        DESCRIPTION.
    params : TYPE
        DESCRIPTION.
    varlabels : TYPE
        DESCRIPTION.

    """
    X, varlabels, exclIdxVec = policyVars(t, policyDates, 'linear', excludeIntervals)
    glmfit = sm.GLM(predMap[glmFamily](y, T, alpha)[~exclIdxVec], X[~exclIdxVec, :],
                    exposure=forwardEq(y, T, alpha)[~exclIdxVec], family=famMap[glmFamily]()).fit()
    ypredres = glmfit.get_prediction(X[~exclIdxVec, :])
    ypred = y * np.nan
    ypred[~exclIdxVec] = invPredMap[glmFamily](ypredres.predicted_mean * forwardEq(y, T, alpha)[~exclIdxVec],
                                               T[~exclIdxVec], alpha)
    params = glmfit.params
    return ypred, params, varlabels


def glmIndivFitRec(y, t, T, policyDates, glmFamily, forwardEq, alphaEqs, excludeIntervals=[], alphaInit=None, nRec=200):
    threshold = 1e-6
    if alphaInit == None:
        alpha = np.random.rand(1)
    else:
        alpha = alphaInit

    X, varlabels, exclIdxVec = policyVars(t, policyDates, 'linear', excludeIntervals)
    for kloop in range(nRec):
        glmfit = sm.GLM(predMap[glmFamily](y, T, alpha)[~exclIdxVec], X[~exclIdxVec, :], \
                        exposure=forwardEq(y, T, alpha)[~exclIdxVec], family=famMap[glmFamily]()).fit()
        ypredres = glmfit.get_prediction(X[~exclIdxVec, :], exposure=(forwardEq(y, T, alpha))[~exclIdxVec])
        Istar = y * np.nan
        Istar[~exclIdxVec] = ypredres.predicted_mean / forwardEq(y, T, alpha)[~exclIdxVec]
        ypred = ypredres.predicted_mean
        alpha_new = alphaEqs(y, ypred, Istar, T, alpha)
        if abs(alpha_new - alpha) < threshold:
            alpha = alpha_new
            break
        alpha = alpha_new
        print(alpha)
    glmfit = sm.GLM(predMap[glmFamily](y, T, alpha)[~exclIdxVec], X[~exclIdxVec, :], \
                    family=famMap[glmFamily]()).fit()
    ypredres = glmfit.get_prediction(X[~exclIdxVec, :])
    ypred = y * np.nan
    ypred[~exclIdxVec] = invPredMap[glmFamily](ypredres.predicted_mean * forwardEq(y, T, alpha)[~exclIdxVec],
                                               T[~exclIdxVec], alpha)

    params = glmfit.params
    return ypred, params, varlabels, alpha


def glmPanelFit(y, t, T, policyDates, glmFamily, forwardEq, excludeIntervals=[], alphaInit=np.ones((1)), debias=2):
    """
    Fit panel data (timex countries)

    Parameters
    ----------
    y : TYPE
        DESCRIPTION.
    t : TYPE
        DESCRIPTION.
    T : TYPE
        DESCRIPTION.
    policyDates : TYPE
        DESCRIPTION.
    glmFamily : TYPE
        DESCRIPTION.
    forwardEq : TYPE
        DESCRIPTION.
    excludeIntervals : TYPE, optional
        DESCRIPTION. The default is [].
    alpha : TYPE, optional
        DESCRIPTION. The default is None.

    Returns
    -------
    ypred : TYPE
        DESCRIPTION.
    params : TYPE
        DESCRIPTION.
    varlabels : TYPE
        DESCRIPTION.

    """
    import torch
    # define policy variables and mean removal matrices
    if debias == 0:
        X, varlabels, exclIdxVec = policyVars(t, policyDates, 'linear', excludeIntervals)
    else:
        X, varlabels, exclIdxVec = policyVars(t, policyDates, 'constant', excludeIntervals)

    M1, M2 = policy_withinMats(t, policyDates, excludeIntervals)

    beta = torch.zeros(X.shape[1], 1, requires_grad=True, dtype=torch.double)

    # remove one data point due to differentiation
    if debias >= 1:
        M1 = np.delete(M1, -1, axis=1)
        M2 = np.delete(M2, -1, axis=0)
        X = np.delete(X, 0, axis=0)

    # convert to double to avoid cast issues
    y = y.astype(np.double)

    # convert relevant variables to torch
    Y = torch.from_numpy(y)
    Te = torch.from_numpy(T)

    # compute transformation eliminating latent heterogeneity
    if debias >= 1:
        deltaY = np.diff(np.log(y), axis=0)
        weights = torch.from_numpy((y[1:] + y[:-1]))
    else:
        deltaY = np.log(y)
        weights = torch.from_numpy(y)
    weights = torch.clamp(weights, 10)
    factors, _ = torch.max(weights, 1, keepdim=True)
    weights = weights / factors
    if debias == 2:
        yRM = deltaY - np.matmul(M2, np.matmul(M1, deltaY))
    else:
        yRM = deltaY

    # convert to torch
    YRM = torch.from_numpy(yRM)

    # set parameter to optimize
    alpha = torch.tensor(alphaInit, requires_grad=True)

    # define loss function
    def loss_fun(teRM):
        return torch.sum(weights * (YRM - teRM) ** 2)

    # initialize optimizer
    if debias == 2:
        optimizer = torch.optim.LBFGS([alpha])
    else:
        optimizer = torch.optim.LBFGS([alpha, beta])

    def closure():
        optimizer.zero_grad()
        teFun = torch.log(forwardEq(Y, Te, torch.exp(alpha)))
        if debias > 0:
            deltaTe = teFun[1:, :] - teFun[:-1, :]
        else:
            deltaTe = teFun
        if debias == 2:
            teRM = deltaTe - torch.matmul(torch.from_numpy(M2).double(),
                                          torch.matmul(torch.from_numpy(M1).double(), deltaTe.double()))
        else:
            teRM = deltaTe
        if debias == 2:
            loss = loss_fun(teRM)
        else:
            loss = loss_fun(teRM + torch.matmul(torch.from_numpy(X).double(), beta))
        loss.backward()
        return loss

    for kite in range(50):
        optimizer.step(closure)
        print(alpha)

    return torch.exp(alpha).detach().numpy()
